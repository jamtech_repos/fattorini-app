import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { MenuComponent } from './components/menu/menu.component';
import { PipesModule } from './pipes/pipes.module';
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import {NgxMaskIonicModule} from 'ngx-mask-ionic';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NgxBarcodeModule } from 'ngx-barcode';
import { Downloader } from '@ionic-native/downloader/ngx';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Network } from '@ionic-native/network/ngx';
import { SimpleMaskModule } from 'ngx-ion-simple-mask'
import { OneSignal } from '@ionic-native/onesignal/ngx';

@NgModule({
  declarations: [AppComponent, MenuComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), SimpleMaskModule, AppRoutingModule, HttpClientModule, PipesModule, IonicStorageModule.forRoot(), NgxMaskIonicModule.forRoot(), NgxBarcodeModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    Crop,
    Clipboard,
    Downloader,
    NetworkInterface,
    PhotoViewer,
    InAppBrowser,
    Network,
    OneSignal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
