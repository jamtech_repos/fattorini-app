import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  hour : string = '';
  selectedValCurrency : string = '';
  mySubscription: any;
  url:string;
  surl:any;
  phone = '+584249749015';

  constructor(private auth: AuthenticationService, public alertCtrl: AlertController, public storage: Storage, 
              public router: Router, public activatedRoute: ActivatedRoute, private dom: DomSanitizer) { 

    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
                
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    this.url = 'whatsapp://send?text=¡Hola! Gracias por contactar con Fattorini Delivery, ¿Qué necesitas?&phone='+this.phone;
    this.surl = this.dom.bypassSecurityTrustUrl(this.url);
    console.log(this.surl, 'whatsapp');
    console.log('init menu component');
  }

  ionViewWillEnter() {
    this.ngOnInit();
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  getHome() {
    this.storage.get('currency').then((val => { 
      this.router.navigate(['/users'], { queryParams: { hour: this.hour, currency: val } });
      this.ngOnInit();
      console.log(val, 'currencyStorage getHome'); 
    } ));
  }

  getOrders() {
    this.storage.get('currency').then((val => { 
      this.router.navigate(['/orderspanel'], { queryParams: { hour: this.hour, currency: val } });
      this.ngOnInit();
      console.log(val, 'currencyStorage getOrders'); 
    } ));
  }

  getCurrencySelector() {
    this.storage.get('currency').then((val => { 
      this.router.navigate(['/currencyselector'], { queryParams: { hour: this.hour, currency: val } });
      this.ngOnInit();
      console.log(val, 'getCurrencySelector()'); 
    } ));
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

}
