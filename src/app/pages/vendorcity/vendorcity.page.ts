import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-vendorcity',
  templateUrl: './vendorcity.page.html',
  styleUrls: ['./vendorcity.page.scss'],
})
export class VendorcityPage implements OnInit {

  hour: string = '';
  listCommerceCity : any = [];
  user: any = [];
  isSkeleton : boolean = false;
  textoBuscar = '';
  currency: string = '';

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, private http: HttpClient, private storage: Storage) { }

  ngOnInit() {
    // Obtiene por parámetro la hora actual para la validación de los horarios del comercio
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency dashboard', this.currency);
      }
    } ));
    this.getUserLogged();
    console.log('ionViewWillEnter toprated');
  }

  listCommercebyCity(city, hour) {
    this.dataservice.getCommercebyCity(city, hour)
    .subscribe(res => {
      this.isSkeleton = true;
      this.listCommerceCity = res;
      console.log(this.listCommerceCity);
    })    
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  getCountCommerce() {
    let count = 0;
    count = this.listCommerceCity.length;
    //console.log('count', count)
    return count;
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    let result: string = await this.storage.get('token');
    const headers = new HttpHeaders().set('Authorization', result);
    this.http.get(SERVER_URL + 'user/loggedin', {headers})
    .subscribe(result => {
        this.user = result;
        this.listCommercebyCity(this.user.city, this.hour);
        console.log('dataUser', this.user);
      },
      err => {
        console.log('ERROR!: ', err);
      }
    );
  } 

}
