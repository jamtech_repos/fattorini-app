import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendorcityPage } from './vendorcity.page';

describe('VendorcityPage', () => {
  let component: VendorcityPage;
  let fixture: ComponentFixture<VendorcityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorcityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendorcityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
