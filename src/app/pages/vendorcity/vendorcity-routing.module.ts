import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorcityPage } from './vendorcity.page';

const routes: Routes = [
  {
    path: '',
    component: VendorcityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorcityPageRoutingModule {}
