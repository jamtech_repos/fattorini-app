import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsmenuPage } from './productsmenu.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsmenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsmenuPageRoutingModule {}
