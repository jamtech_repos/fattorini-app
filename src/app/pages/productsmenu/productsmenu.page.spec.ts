import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsmenuPage } from './productsmenu.page';

describe('ProductsmenuPage', () => {
  let component: ProductsmenuPage;
  let fixture: ComponentFixture<ProductsmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsmenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
