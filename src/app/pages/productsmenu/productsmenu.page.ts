import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import * as numeral from 'numeral';
import { CURRENCY_BS } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-productsmenu',
  templateUrl: './productsmenu.page.html',
  styleUrls: ['./productsmenu.page.scss'],
})
export class ProductsmenuPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;

  page = 0;
  hour: string = '';
  currency: string = '';
  textoBuscar = '';
  dataP : any[] = [];
  isSkeleton : boolean = false;
  products : any[] = [];
  idMenu : string = '';
  idCommerce : string = '';
  idUser : string = '';
  nameMenu : any;
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute,
              public storage: Storage, private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.idMenu = this.activatedRoute.snapshot.queryParamMap.get('menu');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.idCommerce = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.idUser = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.listProductbyMenu(false, "")
  }

  ionViewWillEnter() {
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    //return numeral(price).format('0,0').replace(/,/g,'.');
    return numeral(price).format('0,0.00');
  }

  listProductbyMenu(isFirstLoad, event) {
    this.dataservice.getProductMenu(this.idMenu, this.page, this.currency)
    .subscribe(result => {
      this.isSkeleton = true;
      this.dataP = result;

      let maximumPage = this.dataP['pages'];

      for (const product of result['products']) {
        this.products.push(product);
        this.nameMenu = product.menu.name;
      }

      if(this.page < maximumPage || this.page === 0){
        if(isFirstLoad)
          event.target.complete();
          this.page++;
      } else
          this.infiniteScroll.disabled = true;

        console.log(this.products, 'products');
        console.log('page', this.page);
        console.log('max page', this.dataP);
        console.log(`page=${this.page}, max=${maximumPage}`);
    },
    async (error: HttpErrorResponse) => {
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });      
  }

  loadMoreProducts(event) {
    this.listProductbyMenu(true, event);
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

}
