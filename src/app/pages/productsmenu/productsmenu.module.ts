import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsmenuPageRoutingModule } from './productsmenu-routing.module';

import { ProductsmenuPage } from './productsmenu.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsmenuPageRoutingModule,
    PipesModule
  ],
  declarations: [ProductsmenuPage]
})
export class ProductsmenuPageModule {}
