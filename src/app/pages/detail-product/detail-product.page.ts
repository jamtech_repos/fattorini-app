import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import * as numeral from 'numeral';
import { AlertController, ToastController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { CURRENCY_BS } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
})
export class DetailProductPage implements OnInit {

  public slideOptions = {
    slidesPerView: 1.5,
    centeredSlides: false,
    pagination: false,
    spaceBetween: -10,
    loop: false,
  };

  idSelectedProduct : string = '';
  idSelectedCommerce : string = '';
  idSelectedMenu : string = '';
  data : any[] = [];
  related : any[] = [];
  info : any[] = [];
  menuN : any[] = [];
  listOptions : any[] = [];
  check : any[] = [];
  menuId : any[] = [];
  isSkeleton : boolean = false;
  hour : string = '';
  user : string = '';
  quantity : any = [];
  inCart = null;
  inCartRelated : any = [];
  currency: string = '';
  selectedCheck : boolean = false;
  selectedRadioGroup : any = [];
  valueRadio : any = [];
  valueCheck : any = [];
  totalPrice : number = 0;
  optionsValue : any[] = [];
  options : any[] = [];
  optionsCheck : any[] = [];
  optionsRadio : any[] = [];
  selectedRadioItem : any;
  myToast : any;
  config : any[];
  rangeVal : string;
  _BS = CURRENCY_BS;

  public slideP = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '1',
    centeredSlides: false,
    spaceBetween: 20,
  };

  constructor(private activatedRoute: ActivatedRoute, private dataservice: DataService, private toastCtrl: ToastController,
              private loadingService: LoadingService, private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.idSelectedProduct = this.activatedRoute.snapshot.paramMap.get('id');
    this.idSelectedCommerce = this.activatedRoute.snapshot.paramMap.get('commerce');
    this.idSelectedMenu = this.activatedRoute.snapshot.paramMap.get('menu');
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    console.log(this.user, 'mostrar id user');
  }

  ionViewWillEnter() {
    this.getProductInfo();
    this.getProductMenuRelated();
    this.getQuantityCart();
    this.totalPrice = 0;
    console.log('ionViewWillEnter detail-product');
  }

  // Mostrar la información del producto
  getProductInfo() {
    this.loadingService.loadingPresent();
    this.dataservice.getProductInfo(this.idSelectedProduct, this.currency)
    .subscribe(result => {
      this.isSkeleton = true;
      this.info = result;
      this.rangeVal = result['grmin'];
      this.menuN = result['menu'];
      this.listOptions = result['options'];

      if(this.info['options'].length <= 0) {
        this.getProductInCart();
      }
      
      this.loadingService.loadingDismiss();
      console.log(this.info, 'info product');
      console.log(this.listOptions, 'options');
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });  
  }

  // Mostrar los productos relacionados al menú
  getProductMenuRelated() {
    this.dataservice.listProductMenuRelated(this.idSelectedMenu, this.currency)
    .subscribe(result => {
      this.isSkeleton = true;
      this.menuId = result['products'];
      let x = this.menuId.filter(item => item._id != this.idSelectedProduct);
      this.related = x;
      //console.log(this.menuId, 'menu product related');
      console.log(this.related, 'related filter');
    })
  }

  infoConfig() {
    this.dataservice.getConfigCommerce(this.idSelectedCommerce)
    .subscribe(result => {
      this.config = result;
      console.log(this.config, 'config');
    })   
  }

  getOfferKg(price, rangeVal, percent, totalPrice) {
    let cal = price - price * (percent / 100);
    let result = Number(cal.toString()).toFixed(2);
    let res = (((Number(result)/1000) * rangeVal) + totalPrice);
    return res;
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    //return numeral(price).format('0,0').replace(/,/g,'.');
    return numeral(price).format('0,0.00');
  }

  // Mostrar la cantidad total de productos agregados al carrito
  getQuantityCart() {
    this.dataservice.getTotalCart(this.user)
    .subscribe(result => {
      this.quantity = result;
      console.log('total', this.quantity);
    })
  }

  // Obtener el valor de la cantidad en el carrito del producto si existe
  getProductInCart() {
    this.dataservice.getProductInCart(this.user, this.idSelectedProduct)
    .subscribe(result => {
      this.inCart = result;
      console.log('product in cart', this.inCart);
    })
  }

  // Agregar producto al carrito
  addToCartProduct(idProduct, kg) {
    this.dataservice.addToCart(this.user, idProduct, this.info['options'], kg)
    .subscribe(result => {
      let add = result;
      console.log('addtocart', add);
      
      this.myToast = this.toastCtrl.create({
        message: this.info['name'] + " ha sido añadido a tu carrito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });

      if(this.info['options'].length <= 0) {
        this.getProductInCart();
      }
      
      this.getQuantityCart();
    })
  }

  // Aumentar la cantidad del producto en el carrito
  addProduct(quantity) {

    this.dataservice.getProductInCart(this.user, this.idSelectedProduct)
    .subscribe(result => {
      this.inCart = result;
      console.log('product in cart 2', this.inCart);
      this.dataservice.updateProductCart(this.inCart._id, quantity)
      .subscribe(result => {
        let res = result;
        this.inCart.quantity = quantity;
        console.log('add', res);
  
        this.dataservice.getTotalCart(this.user)
        .subscribe(result => {
          let t = result;
          console.log('total cart add', t);
        })
        this.getQuantityCart();
      })  

    })
  }

  // Reducir la cantidad del producto en el carrito
  removeProduct(quantity) {

    this.dataservice.getProductInCart(this.user, this.idSelectedProduct)
    .subscribe(result => {
      this.inCart = result;

      if(quantity > 0) {
        this.dataservice.updateProductCart(this.inCart._id, quantity)
        .subscribe(result => {
          let res = result;
          this.inCart.quantity = quantity;
          console.log('rm', res);
  
          this.dataservice.getTotalCart(this.user)
          .subscribe(result => {
            let t = result;
            console.log('total cart rm', t);
          })
          this.getQuantityCart();
        })
      }
      console.log('product in cart', this.inCart);
    })
  }

  getSelectedItem(event) {
      this.setOptions();
  }

  radioGroupChange(event, i) {
    let j = event.detail.value;

    for (let k = 0; k < this.info['options'][i].options.length; k++) {
      /* k es el indice de recorrido en la lista */
      /* si k es diferente a j quiere decir que estos no son el que buscamos  */
      /* si son iguales es el que le dimos click y ese cambia a true */
      if (k !== j) this.info['options'][i].options[k].isChecked = false;
      else this.info['options'][i].options[k].isChecked = true;
    }
    this.setOptions();
    console.log(event.detail.value, i, this.info);
  }

  setOptions() {
    let total = 0;
    for (let i = 0; i < this.info['options'].length; i++) {
      const element = this.info['options'][i];
      for (let j = 0; j < element.options.length; j++) {
        const option = element.options[j];
        if (option.isChecked) total += option.value;
      }
    }
    this.totalPrice = total;
    console.log('total price', this.totalPrice);
  }

}
 