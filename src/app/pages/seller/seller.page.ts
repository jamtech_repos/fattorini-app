import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.page.html',
  styleUrls: ['./seller.page.scss'],
})
export class SellerPage implements OnInit {

  listStateCity : any = [];
  cityArray : any = [];
  categorySelected : any;
  category : any = [];
  user : any = [];
  isImage : boolean = false;
  croppedImagePath = '';
  picture : string = '';
  newPicture : any = [];

  contribuyente: any[] = [
    {
      name: 'J',
      
    },
    {
      name: 'G',
    },
    {
      name: 'V',
    },
    {
      name: 'E',
    },
    {
      name: 'P',
    },
  ];

  data = {
    name : '',
    description : '',
    rif : '',
    address : '',
    phone : '',
    category : '',
    state : '',
    city : ''
  }

  selectedContribuyente = 'J';
  selectedRIF = '';

  constructor(private dataservice: DataService, private loadingService: LoadingService,
              private camera : Camera, private activatedRoute: ActivatedRoute, private crop: Crop,
              private navCtrl: NavController, private file: File, private alertCtrl: AlertController,
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.getListStateCity();
    this.getCategoriesList();
  }

  getCategoriesList()
  {
    this.loadingService.loadingPresent();
    this.dataservice.getCategories()
    .subscribe(result => {
      this.category = result;
      this.data.category = this.category[0]._id;
      this.categorySelected = this.category[0].name;
      this.loadingService.loadingDismiss();
      console.log('categories', this.category);
    },
    async (error: HttpErrorResponse) => {
       if(error.status === 0) {
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    }); 
  }

  changeCategory(event) {
    if(event.target.value && event.target.value != '') {
      for (let i = 0; i < this.category.length; i++) {
        const element = this.category[i]._id;
        if(element === event.target.value) {
          this.categorySelected = this.category[i].name;
          console.log(event.target.value);
        }
      }      
    }
  }  

  changeContribuyente(event) {
    this.selectedContribuyente = event.detail.value;
    console.log(this.selectedContribuyente);
  }

  getListStateCity() {
    this.dataservice.getListCountry()
    .subscribe(result => {
      this.listStateCity = result;
      this.data.state = this.listStateCity[0].estado;
      this.data.city = this.listStateCity[0].ciudades[0];
      this.cityArray = this.listStateCity[0].ciudades;

      console.log('list', this.listStateCity);
    });
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.data.state = event.target.value;
      if(this.data.state != 'Desconocido') {
        let c = this.listStateCity.filter(item => item.estado === this.data.state)
        this.cityArray = c[0].ciudades;
        this.data.city = c[0].ciudades[0];
      }
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.data.city = event.target.value;
    }
  }

  popPage() {
    this.navCtrl.pop();
  }

  cropImage(fileUrl) {
    this.crop.crop(fileUrl, { 
        quality: 100,
        targetWidth: 699,
        targetHeight: 700
    }).then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          console.log('Error cropping image' + error);
        }
      );
  }

  showCroppedImage(ImagePath) {
    this.isImage = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagePath = base64;
      this.picture = this.croppedImagePath;
      this.isImage = false;
    }, error => {
      console.log('Error in showing image' + error);
      this.isImage = false;
    });
  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 699,
      targetHeight: 700,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData)
    }, (err) => {
      console.error(err);
    });
  }

  //Actualizar el usuario de tipo C a tipo CA (comercio)
  updateUsertoCommerce() {
    this.loadingService.loadingPresent();
/*     let firstvalue = this.selectedRIF.substring(0, 8);
    let lastvalue = this.selectedRIF.substring(8, 9);
    this.data.rif = this.selectedContribuyente + '-' + firstvalue + '-' + lastvalue; */
    let firstvalue = this.selectedRIF;
    this.data.rif = this.selectedContribuyente + '-' + firstvalue;
    console.log('RIF', this.data.rif);
    this.dataservice.uploadImage(this.picture)
    .subscribe(imageData => {
      this.newPicture = imageData;
      console.log('filebase', this.newPicture);
      this.dataservice.updateRegister(this.user, this.data.name, this.data.description, this.data.rif, 
        this.data.category, this.data.address, this.data.state, this.data.city, this.newPicture.url, this.data.phone)
      .subscribe(async result => {
        let res = result;
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Registro completado',
          message: 'Validaremos tus datos y pronto nos comunicaremos contigo',
          buttons: ['OK']
        });
        await alert.present();
        this.popPage();
        console.log('result update', res);
      },
      async (error: HttpErrorResponse) => {
        this.loadingService.loadingDismiss();
        if(error.status === 0) {
          const alert = await this.alertCtrl.create({
            header: 'Fallo de conexión',
            message: 'Verifique su conexión a Internet e intente de nuevo',
            buttons: ['OK']
          });
          await alert.present();
        }
        else if(error.status === 403) {
          const alert = await this.alertCtrl.create({
            header: 'RIF existente',
            message: error.error.error,
            buttons: ['OK']
          });
          await alert.present();
        }
        console.log(error);
      });
    })  
  }

}
