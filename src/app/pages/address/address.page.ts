import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {

  user : any = [];
  address : any = [];
  isLoader : boolean = false;

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage,
              public alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getUserLogged();
  }

  //Eliminar dirección de la lista
  deleteAddress(id) {
    this.presentAlertConfirm(id);
  }

  removeAddress(id) {
    this.dataservice.removeAddress(id)
    .subscribe(result => {
      let res = result;
      console.log('rm', res);
      this.ionViewWillEnter();
    })    
  }

  async presentAlertConfirm(id) {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea eliminar esta dirección?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.removeAddress(id);
          }
        }
      ]
    });
    await alert.present();
  }

  showAddress(user) {
    this.dataservice.getListUserAddress(user)
    .subscribe(res => {
      this.address = res;
      console.log(this.address, 'direcciones');
    });
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.showAddress(result['_id']);
          console.log(this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });
    }
  } 

}
