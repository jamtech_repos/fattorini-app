import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { SERVER_URL } from 'src/environments/environment';
import { AlertController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  quantity : any = [];
  user: any = [];
  isLoader : boolean = false;
  listStateCity : any = [];
  cityArray : any = [];
  stateSelected : any;
  citySelected : any;
  DateSelected : any;
  nameUser : any;
  myToast : any;
  hour : string = '';
  selectedValCurrency : string = '';

  isoDateStr = '';
  age18 : any;
  showPassword = false;
  passwordToggleIcon = 'eye-outline';
  active : any;

  constructor(private http:HttpClient, private dataservice: DataService, public storage: Storage,
              private toastCtrl: ToastController, private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    this.age18 = moment(new Date()).subtract(18, 'years').format("YYYY-MM-DD");
    console.log(this.age18);
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.selectedValCurrency = val;
      }
    } ));
    this.getUserLogged();
  }

  togglePassword():void {
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye-outline') {
      this.passwordToggleIcon = 'eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }
  }

  cambioFecha(event) {
      this.DateSelected = event.detail.value;
      console.log(this.DateSelected, 'dateselected');
  }

   updateUser(id, firstname, lastname, address, password, state, city, phone, birthday, ci) {
    this.dataservice.updateUserData(id, firstname, lastname, address, password, state, city, phone, birthday, ci)
    .subscribe(result => {
      let res = result;
      this.ionViewWillEnter();

      this.myToast = this.toastCtrl.create({
        message: "Su perfil ha sido actualizado con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });

      console.log(res);
    });
  }

  async userActiveLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Su usuario se encuentra deshabilitado',
      message: 'Ha sido dado de baja o a desactivado su cuenta, contacte con soporte',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            console.log('Yes clicked');
            this.auth.logout();
          }
        }
      ],
      backdropDismiss: false,
    });
      await alert.present();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.stateSelected = this.user.state;
          this.citySelected = this.user.city;
          this.nameUser = this.user.firstname;
          /* const date = moment(this.user.birthday).toDate(); */
          this.DateSelected = this.user.birthday;
  
          console.log('dataUser', this.user, this.DateSelected, 'birthday');
          this.getListStateCity();

          // ¿Usuario activo?
          this.dataservice.getUserActive(this.user._id)
          .subscribe(result => {
            this.active = result;

            if(this.active.status === false) {
              this.userActiveLogout();
            }
            console.log('active user', this.active.status);
          })

        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

  getListStateCity() {
    this.dataservice.getListCountry()
    .subscribe(result => {
      this.listStateCity = result;
   
      if(this.stateSelected === 'Desconocido' && this.citySelected === 'Desconocido') {
          //this.stateSelected = this.listStateCity[0].estado;
          //this.citySelected = this.listStateCity[0].ciudades[0];
          this.cityArray = this.listStateCity[0].ciudades;
      } else {
        let c = this.listStateCity.filter(item => item.estado === this.stateSelected);
        this.cityArray = c[0].ciudades;
      }

    });
    console.log('city', this.stateSelected)
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.stateSelected = event.target.value;
      if(this.stateSelected != 'Desconocido') {
        let c = this.listStateCity.filter(item => item.estado === this.stateSelected)
        this.cityArray = c[0].ciudades;
        this.citySelected = c[0].ciudades[0];
      }
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.citySelected = event.target.value;
    }
  }

}
