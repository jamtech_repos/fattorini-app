import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-tarifas',
  templateUrl: './tarifas.page.html',
  styleUrls: ['./tarifas.page.scss'],
})
export class TarifasPage implements OnInit {

  config : any[] = [];
  zones : any[] = [];
  sector : any[] = [];
  commerce : string = '';
  isLoader: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private dataservice: DataService) { }

  ngOnInit() {
    this.commerce = this.activatedRoute.snapshot.queryParamMap.get('id');
  }

  ionViewWillEnter() {
    this.infoConfig();
  }

  infoConfig() {
    this.dataservice.getConfigCommerce(this.commerce)
    .subscribe(result => {
      this.isLoader = true;
      this.config = result;
      this.zones = result['zones'];
      console.log(this.config, 'config', this.zones, 'zones');
    })   
  }

}
