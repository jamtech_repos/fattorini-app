import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-addaddress',
  templateUrl: './addaddress.page.html',
  styleUrls: ['./addaddress.page.scss'],
})
export class AddaddressPage implements OnInit {

  user : any = [];
  isLoader : boolean = false;
  config : any = [];
  listStateCity : any = [];
  cityArray : any = [];
  zones : any = [];
  z : any = [];
  myToast : any;


  data = {
    state : '',
    city : '',
    address : '',
    sector : ''
  }

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage, 
              private navCtrl: NavController, private toastCtrl: ToastController, private auth: AuthenticationService,
              private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getUserLogged();
    this.showAppConfig();
    this.getListStateCity();
  }

  showAppConfig() {
    this.dataservice.getAppConfig()
    .subscribe(res => {
      this.config = res;
      this.zones = res['zones'];
      console.log(this.config, 'config', this.zones, 'zones');
    });
  }

  getListStateCity() {
    this.dataservice.getListCountry()
    .subscribe(result => {
      this.listStateCity = result;
      this.data.state = this.listStateCity[0].estado;
      this.data.city = this.listStateCity[0].ciudades[0];
      this.cityArray = this.listStateCity[0].ciudades;
      console.log('list', this.listStateCity);
    });
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.data.state = event.target.value;
      if(this.data.state != 'Desconocido') {
        let c = this.listStateCity.filter(item => item.estado === this.data.state)
        this.cityArray = c[0].ciudades;
        this.data.city = c[0].ciudades[0];
      }
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.data.city = event.target.value;
      this.z = this.zones.filter((a) => a.city === this.data.city && a.state === this.data.state)
      .map((a) => a.sectors)
      .flat();
      let sectorsort = this.z.sort((a,b) => {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        return a < b ? -1 : a > b ? 1 : 0;
      })
      if(this.z != 0) {
        this.data.sector = sectorsort[0].name;
      }
      console.log(this.z, 'zones array');
    }
  }

  changeSector(event) {
    if(event.target.value && event.target.value != '') {
      this.data.sector = event.target.value;
    }
  }

  addAddressUser(user) {
    this.dataservice.addAddressUserProfile(user, this.data.state, this.data.city, this.data.sector, this.data.address)
    .subscribe(res => {
      let add = res;
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "Dirección creada con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      console.log(add);
    });
  }

  popPage() {
    this.navCtrl.pop();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          console.log(this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });
    }
  } 

}
