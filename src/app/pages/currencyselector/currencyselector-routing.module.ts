import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrencyselectorPage } from './currencyselector.page';

const routes: Routes = [
  {
    path: '',
    component: CurrencyselectorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrencyselectorPageRoutingModule {}
