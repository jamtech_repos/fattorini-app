import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrencyselectorPageRoutingModule } from './currencyselector-routing.module';

import { CurrencyselectorPage } from './currencyselector.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrencyselectorPageRoutingModule
  ],
  declarations: [CurrencyselectorPage]
})
export class CurrencyselectorPageModule {}
