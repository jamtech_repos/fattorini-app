import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-currencyselector',
  templateUrl: './currencyselector.page.html',
  styleUrls: ['./currencyselector.page.scss'],
})
export class CurrencyselectorPage implements OnInit {

  hour: string = '';
  selectedValCurrency: string = '';
  quantity: any = [];
  user: any = [];
  myToast: any;

  listCurrency: any[] = [{
      id: 1,
      format: 'USD',
      name: 'Dólares ($USD)',
    },
    {
      id: 2,
      format: 'Bs.S',
      name: 'Bolívares Soberanos (Bs.S)',
    }
  ];

  constructor(public storage: Storage, public router: Router, public activatedRoute: ActivatedRoute, 
              private navCtrl: NavController, private toastCtrl: ToastController) {}

  ngOnInit() {
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    console.log('init currencyselector');
  }

  //Cerrar página
  popPage() {
    this.navCtrl.pop();
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda actual del selector
    this.storage.get('currency').then((val => {
      if (val) {
        this.selectedValCurrency = val;
      }
    }));
  }

  //Cambiar moneda
  changeCurrency(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValCurrency = event.target.value;
    }
  }

  //Guardar la moneda seleccionada
  saveCurrency(currency) {
    //Dolar
    if (currency === 'USD') {
      this.setCurrencyDollar(this.selectedValCurrency);
      console.log('dolar');
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "Su moneda actual es " + currency,
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      this.router.navigate(['/users'], {
        queryParams: { hour: this.hour, currency: currency }, replaceUrl: true 
      });
    //Bolívar
    } else if (currency === 'Bs.S') {
      this.setCurrencyBolivar(this.selectedValCurrency);
      console.log('bss');
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "Su moneda actual es " + currency,
        duration: 2000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      this.router.navigate(['/users'], {
        queryParams: { hour: this.hour, currency: currency }, replaceUrl: true
      });
    }
  }

  //Asignar la moneda seleccionada al valor guardado en localstorage
  setCurrencyDollar(format) {
    let dolar = this.storage.set('currency', format);
    this.storage.get('currency').then((val => {
      if (val) {
        console.log(dolar, 'setCurrencyDollar');
      }
    }));
  }

  //Asignar la moneda seleccionada al valor guardado en localstorage
  setCurrencyBolivar(format) {
    let bsS = this.storage.set('currency', format);
    this.storage.get('currency').then((val => {
      if (val) {
        console.log(bsS, 'setCurrencyBolivar');
      }
    }));
  }

}