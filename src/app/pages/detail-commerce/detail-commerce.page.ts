import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as numeral from 'numeral';
import { LoadingService } from 'src/app/services/loading.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-detail-commerce',
  templateUrl: './detail-commerce.page.html',
  styleUrls: ['./detail-commerce.page.scss'],
})
export class DetailCommercePage implements OnInit {

  idSelected : string = '';
  data : any[] = [];
  open : any[] = [];
  profile : any[] = [];
  products : any[] = [];
  category : any[] = [];
  isSkeleton : boolean = false;
  isFavorite: boolean;
  hour: string = '';
  user: string = '';
  currency: string = '';
  page = 0;
  dataP : any[] = [];
  schedule : any[] = [];
  menu : any[] = [];
  textoBuscar = '';

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;

  constructor(private activatedRoute: ActivatedRoute, private dataservice: DataService, private storage: Storage,
              private loadingService: LoadingService, private router: Router, private alertCtrl: AlertController, 
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.idSelected = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    //this.getProductCommerce(false, "");
  }

  ionViewWillEnter() {
    this.getInfoCommerce();
    this.listMenuCommerce();
    console.log('ionViewWillEnter detail commerce');
  }

  getInfoCommerce() {
    this.loadingService.loadingPresent();
    this.dataservice.listInfoCommerceComplete(this.idSelected, this.hour, this.currency)
    .subscribe(result => {
      this.isSkeleton = true;
      //datos completos del comercio
      this.data = result;
      //perfil del comercio
      this.profile = result['commerce'].commerce;
      //horario del comercio
      this.schedule = result['schedule'].schedules;
      //categorías
      this.category = result['commerce'].commerce.category;
      //status abierto o cerrado
      this.open = result['commerce'].open;
      
      this.loadingService.loadingDismiss();
      console.log(this.data, 'info', this.schedule);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        this.loadingService.loadingDismiss();
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }
  
  getProductCommerce(isFirstLoad, event) {
    this.dataservice.listProductCommerce(this.idSelected, this.page, this.hour, this.currency)
    .subscribe(result => {
      this.isSkeleton = true;
      this.dataP = result;
      console.log('res', this.dataP);
  
      let maximumPage = this.dataP['pages'];

      for (const product of result['products']) {
        this.products.push(product);
      }

      if(this.page < maximumPage || this.page === 0){
        if(isFirstLoad)
          event.target.complete();
          this.page++;
      } else
          this.infiniteScroll.disabled = true;

    console.log(this.products, 'products');
    console.log('page', this.page);
    console.log('max page', this.dataP);
    console.log(`page=${this.page}, max=${maximumPage}`);
  })
  }

  loadMoreProducts(event) {
    this.getProductCommerce(true, event);
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  priceFormat(price) {
      return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
      return numeral(price).format('0,0').replace(/,/g,'.');
  }

  listMenuCommerce() {
    this.dataservice.listMenuCommerce(this.idSelected)
    .subscribe(result => {
      this.isSkeleton = true;
      this.menu = result;
      console.log(this.menu, 'menu');
    })   
  }

  filtrarmenu(idMenu) {
    this.router.navigate(['/productsmenu'], { queryParams: { menu:idMenu, currency:this.currency, user:this.user, commerce:this.idSelected} }); 
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  btnFav() {
    this.storage.get('profile').then((val => {
      if(val) {
        var btn = -1;
        let fav = val.findIndex((item, i) => 
        {
          if(item._id == this.idSelected) 
          { 
            btn = i;
            return i; 
          }
        });
        console.log(btn, 'btn');
        this.isFavorite = (btn >= 0);
      } 
      else {
        this.isFavorite = false;
      }
    }))  
  }
}
