import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailCommercePage } from './detail-commerce.page';

describe('DetailCommercePage', () => {
  let component: DetailCommercePage;
  let fixture: ComponentFixture<DetailCommercePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCommercePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailCommercePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
