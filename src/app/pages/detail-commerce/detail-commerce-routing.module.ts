import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailCommercePage } from './detail-commerce.page';

const routes: Routes = [
  {
    path: '',
    component: DetailCommercePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailCommercePageRoutingModule {}
