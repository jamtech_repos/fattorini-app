import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-completeorder',
  templateUrl: './completeorder.page.html',
  styleUrls: ['./completeorder.page.scss'],
})
export class CompleteorderPage implements OnInit {

  hour: string = '';
  currency: string = '';

  constructor(private activatedRoute: ActivatedRoute, private navCtrl: NavController, private router: Router) { }

  ngOnInit() {
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
  }

  popPage() {
    this.navCtrl.pop();
    this.router.navigate(['/orderspanel'], { queryParams: { hour:this.hour, currency:this.currency }, replaceUrl : true });
  }

}
