import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CompleteorderPage } from './completeorder.page';

describe('CompleteorderPage', () => {
  let component: CompleteorderPage;
  let fixture: ComponentFixture<CompleteorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CompleteorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
