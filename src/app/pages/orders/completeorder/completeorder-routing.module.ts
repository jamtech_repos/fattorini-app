import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompleteorderPage } from './completeorder.page';

const routes: Routes = [
  {
    path: '',
    component: CompleteorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompleteorderPageRoutingModule {}
