import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompleteorderPageRoutingModule } from './completeorder-routing.module';

import { CompleteorderPage } from './completeorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompleteorderPageRoutingModule
  ],
  declarations: [CompleteorderPage]
})
export class CompleteorderPageModule {}
