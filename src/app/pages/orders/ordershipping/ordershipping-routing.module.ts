import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdershippingPage } from './ordershipping.page';

const routes: Routes = [
  {
    path: '',
    component: OrdershippingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdershippingPageRoutingModule {}
