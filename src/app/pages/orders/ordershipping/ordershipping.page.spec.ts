import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdershippingPage } from './ordershipping.page';

describe('OrdershippingPage', () => {
  let component: OrdershippingPage;
  let fixture: ComponentFixture<OrdershippingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdershippingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdershippingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
