import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';
import * as moment from 'moment';
import * as numeral from 'numeral';
import { LoadingService } from 'src/app/services/loading.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-ordershipping',
  templateUrl: './ordershipping.page.html',
  styleUrls: ['./ordershipping.page.scss'],
})
export class OrdershippingPage implements OnInit {

  config : any = [];
  info : any = [];
  order : any = [];
  address : any = [];
  user : any = [];
  products : any = [];
  detail : any = [];
  id : string = '';
  currency : string = '';
  commerce : string = '';
  hour : string = '';
  isLoader : boolean = false;
  dataDir: any;
  selectedDir: any;
  citySelected : any;
  stateSelected: any;
  sectorSelected : any;
  addressSelected : any;
  listStateCity : any = [];
  cityArray : any = [];
  zones : any = [];
  z : any = [];
  tarifa : any = [];
  myToast : any;
  isChecked : boolean = false;
  products_amount : any;
  isShipping : any;
  commision : any;
  contador : any;
  isAddress : boolean = false;
  isPerson : boolean = false;
  nameSelected : any;
  idSelected : any;

  constructor(private dataservice: DataService, private navCtrl: NavController, public alertCtrl: AlertController, private router: Router,
              private activatedRoute: ActivatedRoute, private http: HttpClient, private storage: Storage,
              private loadingService: LoadingService, private auth: AuthenticationService) { }

  ngOnInit() {
    this.commerce = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.id = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.order = this.activatedRoute.snapshot.queryParamMap.get('order');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    this.infoOrderUser();
    this.getUserLogged();
    this.showOrderDetail();
    this.infoConfig();
    console.log('init order shipping');
  }

  popPage() {
    this.navCtrl.pop();
  }

  changeShipping(event) {
    this.isShipping = event;
    console.log(this.isShipping, 'isShipping');
  }

  changePerson(event) {
    this.isPerson = event;
    if(this.isPerson) {
      this.nameSelected = '';
      this.idSelected = '';
    } else {
      this.nameSelected = '';
      this.idSelected = '';
    }
    console.log(this.isPerson, 'isPerson');
  }

  infoConfig() {
    this.dataservice.getConfigCommerce(this.commerce)
    .subscribe(result => {
      this.info = result;
      console.log(this.info, 'info config');
    })   
  }

  flatten(array)
  {
      if(array.length == 0)
          return array;
      else if(Array.isArray(array[0]))
          return this.flatten(array[0]).concat(this.flatten(array.slice(1)));
      else
          return [array[0]].concat(this.flatten(array.slice(1)));
  }

  //Procesar pedido
   infoOrderUser() {
    this.loadingService.loadingPresent();
    this.dataservice.getConfigCommerce(this.commerce)
    .subscribe(result => {
      this.zones = result['zones'];

      this.dataservice.getOrderPaymentDetail(this.order, 'USD')
      .subscribe(res => {
        this.config = res;
        console.log('detail payment config', this.config);
      });

      this.dataservice.getListUserAddress(this.id)
      .subscribe(res => {
        this.isLoader = true;
        this.address = res;
        if(this.address.length > 0) {
          this.dataDir = this.address[0]._id;
          this.sectorSelected = this.address[0].sector;
          this.citySelected = this.address[0].city;
          this.stateSelected = this.address[0].state;
          this.addressSelected = this.address[0].address;
          this.selectedDir = this.sectorSelected + ', ' + this.citySelected + ', ' + this.stateSelected + ', ' + this.addressSelected;
        }
  
        this.dataservice.getListCountry()
        .subscribe(result => {
          this.listStateCity = result;

          if(this.address.length > 0) {
            let c = this.listStateCity.filter(item => item.estado === this.stateSelected);
            this.cityArray = c[0].ciudades;
            console.log('list', this.listStateCity);
    
            this.z = this.flatten(this.zones.filter((a) => a.city === this.citySelected && a.state === this.stateSelected)
            .map((a) => a.sectors));
  
            this.sectorSelected = this.z.filter((a) => a.name === this.address[0].sector).length > 0
            ? this.address[0].sector
            : 'Coloque un sector disponible';
            
            let sectorsort = this.z.sort((a,b) => {
              a = a.name.toLowerCase();
              b = b.name.toLowerCase();
              return a < b ? -1 : a > b ? 1 : 0;
            })

          /* asignar comision */
          let sector = this.z.filter((a) => a.name === this.address[0].sector);
          if (sector.length > 0) {
            let zone = this.zones.filter(
              (a) => a.sectors.filter((b) => b.name === sector[0].name).length > 0
            );
            this.tarifa = zone[0].rate
                ? zone[0].rate.price
                : zone[0].rate_number;
          }
            console.log('zones array', this.z, 'sector', this.sectorSelected, this.tarifa, 'tarifa infoOrderUser');
          }
        });
        this.loadingService.loadingDismissOrder();
        console.log(this.address, 'direcciones');
      });
    });
  }

  changeAddress(event) {
    if(event.target.value && event.target.value != '') {
      for (let i = 0; i < this.address.length; i++) {
        const element = this.address[i]._id;
        if(element === event.target.value) {
          this.sectorSelected = this.address[i].sector;
          this.citySelected = this.address[i].city;
          this.stateSelected = this.address[i].state;
          this.addressSelected = this.address[i].address;
          this.selectedDir = this.sectorSelected + ', ' + this.citySelected + ', ' + this.stateSelected + ', ' + this.addressSelected;

          this.z = this.flatten(this.zones.filter((a) => a.city === this.citySelected && a.state === this.stateSelected)
          .map((a) => a.sectors));

          this.sectorSelected = this.z.filter((a) => a.name === this.address[i].sector).length > 0
          ? this.address[i].sector
          : 'Coloque un sector disponible';
          
          let sectorsort = this.z.sort((a,b) => {
            a = a.name.toLowerCase();
            b = b.name.toLowerCase();
            return a < b ? -1 : a > b ? 1 : 0;
          })

          /* asignar comision */
          let sector = this.z.filter((a) => a.name === this.address[0].sector);
          if (sector.length > 0) {
            let zone = this.zones.filter(
              (a) => a.sectors.filter((b) => b.name === sector[0].name).length > 0
            );
            this.tarifa = zone[0].rate
                ? zone[0].rate.price
                : zone[0].rate_number;
          }
          console.log('zones array', this.z, 'sector', this.sectorSelected, this.tarifa, 'tarifa changeAddres');
          console.log(event.target.value, 'value');
        }
      }      
    }
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.stateSelected = event.target.value;
      if(this.stateSelected != 'Desconocido') {
        let c = this.listStateCity.filter(item => item.estado === this.stateSelected)
        this.cityArray = c[0].ciudades;
        this.citySelected = c[0].ciudades[0];
      }
      this.isAddress = true;
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.citySelected = event.target.value;
      this.z = this.flatten(this.zones.filter((a) => a.city === this.citySelected && a.state === this.stateSelected)
      .map((a) => a.sectors));
      let sectorsort = this.z.sort((a,b) => {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        return a < b ? -1 : a > b ? 1 : 0;
      })
      if(this.z != 0) {
        this.sectorSelected = sectorsort[0].name;

        let zone = this.zones.filter(
          (a) => a.sectors.filter((b) => b.name === this.sectorSelected).length > 0
        );
        this.tarifa = zone[0].rate
        ? zone[0].rate.price
        : zone[0].rate_number;

      }

      this.isAddress = true;
      console.log(this.z, 'zones array');
    }
  }

  changeSector(event) {
    if(event.target.value && event.target.value != '') {
      this.sectorSelected = event.target.value;
      let zone = this.zones.filter(
        (a) => a.sectors.filter((b) => b.name === this.sectorSelected).length > 0
      );
      this.tarifa = zone[0].rate
      ? zone[0].rate.price
      : zone[0].rate_number;
      
      this.isAddress = true;
      console.log(zone, 'zone commision', this.zones, this.tarifa, 'tarifa changeSector', this.sectorSelected);
    }
  }

  //Contador de caracteres
  onKey(event){
    if(event.isTrusted) {
      this.isAddress = true;
    }
    console.log(event, 'evento');
  }

  addNewAddress(event) {
    if (this.isChecked) {
      event.checked = true;
      console.log(this.isChecked);
    } else {
      event.checked = false;
      console.log(this.isChecked);
    }
  }

  stepOrderPayment(order, commerce, user, delivery_amount, delivery, note, name, rif) {
    this.dataservice.updateOrderShipping(order, delivery, 1, this.stateSelected, this.citySelected, this.sectorSelected, this.addressSelected, this.user.phone, note, name, rif)
    .subscribe(res => {
      let updateShipping = res;
      console.log('next', updateShipping);

      this.dataservice.getPaymentDetail(order, commerce, user, delivery_amount) 
      .subscribe(result => {
        let orderpayment = result;
        this.popPage();
        this.router.navigate(['/orderpayment'], { queryParams: { order:order, commerce:commerce, currency:this.currency } });
        console.log(orderpayment);
      });

      if(this.isChecked) {
        this.dataservice.addAddressUserProfile(this.id, this.stateSelected, this.citySelected, this.sectorSelected, this.addressSelected)
        .subscribe(res => {
          let add = res;
          console.log(add);
        });
        console.log('agrego dirección', this.isChecked);
      } else {
        console.log('dirección actual', this.isChecked);
      }

    });
  }
  
  stepOrderPaymentPickup(order, commerce, user, delivery, name, rif) {
    this.dataservice.updateOrderShipping(order, delivery, 1, '', '', '', '', this.user.phone, '', name, rif)
    .subscribe(res => {
      let updateShipping = res;
      console.log('next', updateShipping);

      this.dataservice.getPaymentDetail(order, commerce, user, 0) 
      .subscribe(result => {
        let orderpayment = result;
        this.popPage();
        this.router.navigate(['/orderpayment'], { queryParams: { order:order, commerce:commerce, currency:this.currency } });
        console.log(orderpayment);
      });
    });
  }


  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.idSelected = this.user.ci;
          this.nameSelected = this.user.firstname + ' ' + this.user.lastname;
          console.log(this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

  getOffer(price, percent) {
    let result = price * (percent / 100);
    return price - result;
  };

  getGrams(price, gr) {
    return (price / 1000) * gr;
  }

  showOrderDetail() {
    this.dataservice.getOrderDetails(this.order, 'USD')
    .subscribe(res => {
      this.detail = res;
      this.products = this.detail['products'];
      console.log('order detail', this.detail, 'products', this.products);
    });
  }

   getSubtotal() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOffer(element.price, element['product'].offerpercent)), element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += option.value * element.quantity;
          }        
      } else {
        sum += this.getOffer(element.price, element['product'].offerpercent) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += option.value * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum + sumoption)
    return sum + sumoption;
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    return numeral(price).format('0,0').replace(/,/g,'.');
  }

}
