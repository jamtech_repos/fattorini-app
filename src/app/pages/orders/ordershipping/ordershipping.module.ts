import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdershippingPageRoutingModule } from './ordershipping-routing.module';

import { OrdershippingPage } from './ordershipping.page';
import { SimpleMaskModule } from 'ngx-ion-simple-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdershippingPageRoutingModule,
    SimpleMaskModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [OrdershippingPage]
})
export class OrdershippingPageModule {}
