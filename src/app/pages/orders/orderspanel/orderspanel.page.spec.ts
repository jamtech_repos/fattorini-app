import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderspanelPage } from './orderspanel.page';

describe('OrderspanelPage', () => {
  let component: OrderspanelPage;
  let fixture: ComponentFixture<OrderspanelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderspanelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderspanelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
