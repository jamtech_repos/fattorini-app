import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { CURRENCY_BS, SERVER_URL } from 'src/environments/environment';
import * as moment from 'moment';
import * as numeral from 'numeral';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-orderspanel',
  templateUrl: './orderspanel.page.html',
  styleUrls: ['./orderspanel.page.scss'],
})
export class OrderspanelPage implements OnInit {

  user : any = [];
  orders : any = [];
  detailpayment : any = [];
  totalorder : any = [];
  orderid : any = [];
  config : any = [];
  address : any = [];
  products : any = [];
  isLoader : boolean = false;
  textoBuscar = '';
  currency = '';
  deliveryman : any = [];
  selectedDeliveryMan: any = [];
  moment: any = moment;
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage,
              public alertCtrl: AlertController, private router: Router, private activatedRoute: ActivatedRoute,
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    console.log('init orderspanel');
  }

  ionViewWillEnter() {
    this.getUserLogged();
    console.log('ionViewWillEnter orderspanel');
  }

  // Actualizar la vista
  doRefresh(event) {
    setTimeout(() => {
      this.ionViewWillEnter();
      event.target.complete();
    }, 2000);
  }

  listOrders(user) {
    this.dataservice.getListUserOrders(user, this.currency)
    .subscribe(res => {
      this.orders = res;

      //consultar el repartidor y asociar el ID con su nombre de pila
      this.dataservice.getDeliveryman()
      .subscribe(res => {
        this.deliveryman = res;
        console.log(this.deliveryman, 'deliveryman')

        //iterar las ordenes para sacar el id del repartidor
        for (let i = 0; i < this.orders.length; i++) {
          const element1 = this.orders[i].delivery_man;
          //iterar el array de los repartidores para sacar los id
          for (let j = 0; j < this.deliveryman.length; j++) {
            const element2 = this.deliveryman[j]._id;
          //comparar ids de ordenes y repartidores
          if(element1 === element2) {
            this.selectedDeliveryMan = this.deliveryman[j].user.firstname + ' ' + this.deliveryman[j].user.lastname;
          }
        }}
      });    
      console.log(this.orders, 'orders', this.selectedDeliveryMan, 'selectdeliveryman');
  });
  }

  sumQty(orders) {
    let qty = 0;
    for (let i = 0; i < orders.length; i++) {
       qty += orders[i].quantity;
    }
    return qty;
  }

  //Cancelar pedido
  cancelOrder(id) {
    this.presentAlertConfirm(id);
  }

  removeOrder(id) {
    this.dataservice.changeStatusOrder(id, 10)
    .subscribe(result => {
      let res = result;
      console.log('rm', res);
      this.ionViewWillEnter();
    })    
  }

  async presentAlertConfirm(id) {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cancelar este pedido?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.removeOrder(id);
          }
        }
      ]
    });
    await alert.present();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.listOrders(result['_id']);
          console.log(this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });
    }
  } 

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  priceFormat(price) {
      return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
      return numeral(price).format('0,0').replace(/,/g,'.');
  }

  getOffer(price, percent) {
    let result = price * (percent / 100);
    return price - result;
  };

  setTotalOptions(options) {
    let total = 0;
    let arrayvalue = options.map((a) => a.value);
    for (const item of arrayvalue) {
      total += item;
    }
    return total;
  }

  setSupTotalProduct(product, quantity, price, options, grams) {
    let priceres = product.offer
      ? this.getOffer(price, product.offerpercent)
      : price;
    if (product.pricebykg) {
      return (
        ((priceres * grams) / 1000 + this.setTotalOptions(options)) * quantity
      );
    } else {
      return (priceres + this.setTotalOptions(options)) * quantity;
    }
  }

  getSubTotal(products) {
    let value = 0;
    for (const element of products) {
      value += this.setSupTotalProduct(
        element.product,
        element.quantity,
        element.price,
        element.options,
        element.grams
      );
    }
    return value;
  }

  getDetailPayment(order) {
    this.dataservice.getOrderPaymentDetail(order, 'USD')
    .subscribe(res => {
      this.detailpayment = res;
      //this.detailorder = res['order']._id;
      console.log('detail payment', this.detailpayment);
    });
  }

}
