import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderspanelPage } from './orderspanel.page';

const routes: Routes = [
  {
    path: '',
    component: OrderspanelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderspanelPageRoutingModule {}
