import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderpaymentPageRoutingModule } from './orderpayment-routing.module';

import { OrderpaymentPage } from './orderpayment.page';
import { PayPal } from '@ionic-native/paypal/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderpaymentPageRoutingModule
  ],
  providers: [
    PayPal
  ],
  declarations: [OrderpaymentPage]
})
export class OrderpaymentPageModule {}
