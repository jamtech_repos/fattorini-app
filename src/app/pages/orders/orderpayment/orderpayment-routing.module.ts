import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderpaymentPage } from './orderpayment.page';

const routes: Routes = [
  {
    path: '',
    component: OrderpaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderpaymentPageRoutingModule {}
