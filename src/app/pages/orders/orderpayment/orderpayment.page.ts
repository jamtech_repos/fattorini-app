import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment';
import * as numeral from 'numeral';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoadingService } from 'src/app/services/loading.service';
import { PayPal, PayPalPayment, PayPalConfiguration, PayPalPaymentDetails } from '@ionic-native/paypal/ngx';
import { CURRENCY_BS } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-orderpayment',
  templateUrl: './orderpayment.page.html',
  styleUrls: ['./orderpayment.page.scss'],
})
export class OrderpaymentPage implements OnInit {

  currency : string = '';
  order : string = '';
  commerce : string = '';
  detailpayment : any = [];
  detail : any = [];
  datacommerce : any = [];

  bank : any = [];
  bankdata : any = [];
  dataBank : any = [];
  dataZelle : any = [];
  dataPM : any = [];
  dataPaypal : any = [];
  dataEfectivo : any = [];

  bankvalue: any[] = [];
  hour : any;
  dataPayment: any;
  selectedBank : any;
  selectedPM : any;
  selectedPaypal : any;
  selectedPayment : any;
  imagePath : any;
  comprobante : any;

  transf : any = [];
  pm : any = [];
  paypalM : any = [];
  zelle : any = [];
  efectivo : any = [];

  infoBank : any = [];
  infoPM : any = [];
  infoPaypal : any = [];
  infoZelle : any = [];
  infoEfectivo : any = [];
  inf : any = [];
  isLoader : boolean = false;
  myToast : any;
  myArr : any = [];
  newArr : any = [];
  products : any = [];
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, private navCtrl: NavController, public alertCtrl: AlertController, 
              private loadingService: LoadingService, private activatedRoute: ActivatedRoute, public clipboard: Clipboard, 
              private toastCtrl: ToastController, private camera : Camera, private router: Router, private payPal: PayPal,
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.order = this.activatedRoute.snapshot.queryParamMap.get('order');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.commerce = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    this.showOrderDetail();
    this.showPaymentData();
  }

  popPage() {
    this.navCtrl.pop();
  }

  goOrders() {
    this.popPage();
    this.router.navigate(['/orderspanel'], { queryParams: { hour:this.hour, currency:this.currency }, replaceUrl : true });
  }

  showOrderDetail() {
    this.dataservice.getOrderDetails(this.order, 'USD')
    .subscribe(res => {
      this.detail = res;
      this.datacommerce = this.detail['commerce'];
      this.products = this.detail['products'];
      console.log('order detail', this.detail);

      this.dataservice.getOrderPaymentDetail(this.order, 'USD')
      .subscribe(res => {
        this.detailpayment = res;
        console.log('detail payment', this.detailpayment);
      });

    });
  }

  showPaymentData() {
    this.dataservice.getBankData(this.commerce)
    .subscribe(res => {
      this.bankdata = res;
      this.isLoader = true;
      if(this.bankdata != '') {
         this.bank = this.bankdata.reduce((r, a) => {
          r[parseInt(a.type) - 1] = [...(r[parseInt(a.type) - 1] || []), a];
          return r;
        }, []);

        const myObj = {}

        this.myArr = this.bankdata.map((a) => a.type);
        this.myArr.forEach(el => !(el in myObj) && (myObj[el] = true) && this.newArr.push(el))

        console.log(this.newArr, 'tipos');
  
        //Datos internos de cada metodo de pago
        this.transf = this.bank[0];
        this.pm = this.bank[1];
        this.paypalM = this.bank[2];
        this.zelle = this.bank[3];
        this.efectivo = this.bank[4];
  
        //Inicializar los desplegables
  
        //Transferencia
        if(this.transf != undefined) {
          this.selectedBank = this.transf[0].bank;
          this.dataBank = this.transf[0]._id;
          this.getInfoBank(this.dataBank);
        }
  
        //Pago móvil
        if(this.pm != undefined) {
          this.selectedPM = this.pm[0].bank;
          this.dataPM = this.pm[0]._id;
          this.getInfoPM(this.dataPM);
        }

        //Paypal
        if(this.paypalM != undefined) {
          this.dataPaypal = this.paypalM[0]._id;
          this.getInfoPaypal(this.dataPaypal);
        }

        //Zelle
        if(this.zelle != undefined) {
          this.dataZelle = this.zelle[0]._id;
          this.getInfoZelle(this.dataZelle);
        }

        //Efectivo
        if(this.efectivo != undefined) {
          this.dataEfectivo = this.efectivo[0]._id;
          this.getInfoEfectivo(this.dataEfectivo);
        }

      }

      console.log('bankdata', this.bank, 'transf', this.transf, 'pm', this.pm, 'paypal', this.paypalM);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });   
  }

  getInfoBank(id) {
    for (let i = 0; i < this.transf.length; i++) {
      const element = this.transf[i]._id;
      if(element === id) {
        this.infoBank = this.transf[i];
      }
    }
    console.log(this.infoBank, 'info bank');
  }

  getInfoPM(id) {
    for (let i = 0; i < this.pm.length; i++) {
      const element = this.pm[i]._id;
      if(element === id) {
        this.infoPM = this.pm[i];
      }
    }
    console.log(this.infoPM, 'info pago movil');
  }

  getInfoPaypal(id) {
    for (let i = 0; i < this.paypalM.length; i++) {
      const element = this.paypalM[i]._id;
      if(element === id) {
        this.infoPaypal = this.paypalM[i];
      }
    }
    console.log(this.infoPaypal, 'info paypal');
  }

  getInfoZelle(id) {
    for (let i = 0; i < this.zelle.length; i++) {
      const element = this.zelle[i]._id;
      if(element === id) {
        this.infoZelle = this.zelle[i];
      }
    }
    console.log(this.infoZelle, 'info zelle');
  }

  getInfoEfectivo(id) {
    for (let i = 0; i < this.efectivo.length; i++) {
      const element = this.efectivo[i]._id;
      if(element === id) {
        this.infoEfectivo = this.efectivo[i];
      }
    }
    console.log(this.infoEfectivo, 'info efectivo');
  }

   setNameBank(index) {
    switch (index) {
      case 0:
        return 'Transferencia Bancaria';
      case 1:
        return 'Pago móvil';
      case 2:
        return 'Paypal';
      case 3:
        return 'Zelle';
      default:
        break;
    }
  }

  copyBank(bank, accountnumber, ci, phone) {
    let text = bank + `\n` + accountnumber + `\n` + ci + `\n` + phone;
    this.clipboard.copy(text).then(() => {
      this.myToast = this.toastCtrl.create({
        message: "Copiado con éxito",
        duration: 2000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
    })
  }

  copyPM(bank, ci, phone) {
    let text = bank + `\n` + ci + `\n` + phone;
    this.clipboard.copy(text).then(() => {
      this.myToast = this.toastCtrl.create({
        message: "Copiado con éxito",
        duration: 2000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
    })
  }

  changePayment(event) {
    if(event.target.value && event.target.value != '') {
      this.dataPayment = event.target.value;
      if(this.dataPayment === 0) {
        this.selectedBank = this.transf[0];
      }
      if(this.dataPayment === 1) {
        this.selectedPM = this.pm[0];
      }
      console.log('dataPayment', this.dataPayment);
    }
  }

  changeBank(event) {
    if(event.target.value && event.target.value != '') {
      this.dataBank = event.target.value;

      for (let i = 0; i < this.transf.length; i++) {
        const element = this.transf[i]._id;
        
        if(element === event.target.value) {
          this.selectedBank = this.transf[i].bank;
        }
      }
      this.getInfoBank(this.dataBank);
      console.log(this.dataBank);
    }
  }

  changePM(event) {
    if(event.target.value && event.target.value != '') {
      this.dataPM = event.target.value;

      for (let i = 0; i < this.pm.length; i++) {
        const element = this.pm[i]._id;
        
        if(element === event.target.value) {
          this.selectedPM = this.pm[i].bank;
        }
      }
      this.getInfoPM(this.dataPM);
      console.log('dataPM', this.dataPM);
    }
  }

  priceFormat(price) {
      return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price, exchange_rate) {
    if(this.detailpayment != '') {
      let dolarToBs = numeral(price).format('0,0.00') * exchange_rate;
      return numeral(dolarToBs).format('0,0').replace(/,/g,'.');
    }
  }

  /* Comprobante */
  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imagePath = 'data:image/jpeg;base64,' + imageData;
      console.log(this.imagePath);
    }, (err) => {
      console.error(err);
    });
  }

  checkout(id, bankdata, proof_of_payment, reference) {
    this.loadingService.loadingComprobante();
    this.dataservice.uploadImage(proof_of_payment)
    .subscribe(imageData => {
      this.comprobante = imageData;
      console.log('filebase', this.comprobante);
      this.dataservice.orderReady(id, bankdata, this.comprobante.url, reference.toString(), 1, '', '', '')
      .subscribe(result => {
        let res = result;
        this.dataservice.updateOrderStatus(this.order, 2)
        .subscribe(status => {
          let st = status;
          console.log('result update banco', res, st);
        })
        this.loadingService.loadingDismiss();
        this.popPage();
        //this.router.navigateByUrl('/completeorder',{ replaceUrl : true });
        this.router.navigate(['/completeorder'], { queryParams: { currency:this.currency }, replaceUrl : true });
      });
    });
  }

  checkoutPM(id, bankdata, proof_of_payment) {
    this.loadingService.loadingComprobante();
    this.dataservice.uploadImage(proof_of_payment)
    .subscribe(imageData => {
      this.comprobante = imageData;
      console.log('filebase', this.comprobante);
      this.dataservice.orderReadyPM(id, bankdata, this.comprobante.url, 1)
      .subscribe(result => {
        let res = result;
        this.dataservice.updateOrderStatus(this.order, 2)
        .subscribe(status => {
          let st = status;
          console.log('result update pago movil', res, st);
        })
        this.loadingService.loadingDismiss();
        this.popPage();
        //this.router.navigateByUrl('/completeorder',{ replaceUrl : true });
        this.router.navigate(['/completeorder'], { queryParams: { currency:this.currency }, replaceUrl : true });
      });
    });
  }

  checkoutPaypal(id, client_id, amount) {
    this.payPal.init({
      PayPalEnvironmentProduction: '',
      //PayPalEnvironmentSandbox: 'AQpmeWXa2pmGndYdeOR2p4EjH3UbMTjSiGjIrJ3WdA6XetWxloqXkoiXZEHqRa4Vq22mV1EGk13Zzcv8'
      PayPalEnvironmentSandbox: client_id
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        acceptCreditCards: true,
        languageOrLocale: 'es-ES',
        merchantName: 'Fattorini',
        merchantPrivacyPolicyURL: '',
        merchantUserAgreementURL: '',
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let detail = new PayPalPaymentDetails(amount, '0.00', '0.00');
        let payment = new PayPalPayment(amount, 'USD', 'Fattorini', 'sale', detail);
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          console.log(res, 'res paypal');
          this.loadingService.loadingPresent();
          this.dataservice.orderReady(id, this.dataPaypal, '', res.response.id, 1, '', '', '')
          .subscribe(result => {
            let res = result;
            this.dataservice.updateOrderStatus(this.order, 2)
            .subscribe(status => {
              let st = status;
              console.log('result update paypal', res, st);
            })
            this.loadingService.loadingDismiss();
            this.popPage();
            //this.router.navigateByUrl('/completeorder',{ replaceUrl : true });
            this.router.navigate(['/completeorder'], { queryParams: { currency:this.currency }, replaceUrl : true });
          });
          

          console.log('pago realizado');
          // Successfully paid
    
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          console.log('error, no se realizó el pago');
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }

  checkoutZelle(id, bankdata, proof_of_payment, name, email) {
    this.loadingService.loadingComprobante();
    this.dataservice.uploadImage(proof_of_payment)
    .subscribe(imageData => {
      this.comprobante = imageData;
      console.log('filebase', this.comprobante);
      this.dataservice.orderReady(id, bankdata, this.comprobante.url, '', 1, name, email, '')
      .subscribe(result => {
        let res = result;
        this.dataservice.updateOrderStatus(this.order, 2)
        .subscribe(status => {
          let st = status;
          console.log('result update zelle', res, st);
        })
        this.loadingService.loadingDismiss();
        this.popPage();
        this.router.navigate(['/completeorder'], { queryParams: { currency:this.currency }, replaceUrl : true });
      });
    });
  }

  checkoutEfectivo(id, bankdata, proof_of_payment, description) {
    this.loadingService.loadingComprobante();
    this.dataservice.uploadImage(proof_of_payment)
    .subscribe(imageData => {
      this.comprobante = imageData;
      console.log('filebase', this.comprobante);
      this.dataservice.orderReady(id, bankdata, this.comprobante.url, '', 1, '', '', description)
      .subscribe(result => {
        let res = result;
        this.dataservice.updateOrderStatus(this.order, 2)
        .subscribe(status => {
          let st = status;
          console.log('result update efectivo', res, st);
        })
        this.loadingService.loadingDismiss();
        this.popPage();
        this.router.navigate(['/completeorder'], { queryParams: { currency:this.currency }, replaceUrl : true });
      });
    });
  }

  getOffer(price, percent) {
    let result = price * (percent / 100);
    return price - result;
  };

  getGrams(price, gr) {
    return (price / 1000) * gr;
  }

  getSubtotal() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOffer(element.price, element['product'].offerpercent)), element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += option.value * element.quantity;
          }        
      } else {
        sum += this.getOffer(element.price, element['product'].offerpercent) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += option.value * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum + sumoption)
    return sum + sumoption;
  }

}
