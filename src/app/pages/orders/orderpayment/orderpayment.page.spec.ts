import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderpaymentPage } from './orderpayment.page';

describe('OrderpaymentPage', () => {
  let component: OrderpaymentPage;
  let fixture: ComponentFixture<OrderpaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderpaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderpaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
