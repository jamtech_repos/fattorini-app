import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import * as numeral from 'numeral';
import * as moment from 'moment';
import { LoadingService } from 'src/app/services/loading.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { CURRENCY_BS } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-orderdetails',
  templateUrl: './orderdetails.page.html',
  styleUrls: ['./orderdetails.page.scss'],
})
export class OrderdetailsPage implements OnInit {

  currency : string = '';
  order : string = '';
  detail : any = [];
  products : any = [];
  commerce : any = [];
  config : any = [];
  detailpayment : any = [];
  bankdata : any = [];
  isLoader : boolean = false;
  idCA : string = '';
  options : any = [];
  deliveryman : any = [];
  selectedDeliveryMan : any = [];
  moment: any = moment;
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, public alertCtrl: AlertController, private photoViewer: PhotoViewer,
              private activatedRoute: ActivatedRoute, private auth: AuthenticationService) { }

  ngOnInit() {
    this.order = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.idCA = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.showOrderDetail();
  }

  showImg(url) {
    this.photoViewer.show(url);
  }

  showOrderDetail() {
    //this.loadingService.loadingOrder();
    this.dataservice.getOrderDetails(this.order, 'USD')
    .subscribe(res => {
      this.detail = res;
      this.commerce = this.detail['commerce'];
      this.products = this.detail['products'];
      const de = res['delivery_man'];

      //consultar el repartidor y asociar el ID con su nombre de pila
      this.dataservice.getDeliveryman()
      .subscribe(res => {
        this.deliveryman = res;
        console.log(this.deliveryman, 'deliveryman')

        //iterar las ordenes para sacar el id del repartidor
        for (let i = 0; i < this.deliveryman.length; i++) {
          const element = this.deliveryman[i]._id;
          console.log(element, de, 'ver aqui')
          //comparar ids de ordenes y repartidores
          if(element === de) {
            this.selectedDeliveryMan = this.deliveryman[i].user.firstname + ' ' + this.deliveryman[i].user.lastname;
          }
        }
      });

      this.dataservice.getOrderPaymentDetail(this.order, this.currency)
      .subscribe(res => {
        this.detailpayment = res;
        if(this.detail.status >= 2) {
          this.bankdata = this.detailpayment['bankdata'];
        }
        this.isLoader = true;
        //this.loadingService.loadingDismissOrder();
        console.log('detail payment', this.detailpayment, this.bankdata, 'bankdata');
      });
      console.log('order detail', this.detail);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  //Dólares
  priceFormatProduct(price) {
    return numeral(price).format('0,0.00');
  }

  //Bolívares
  priceFormatTasa(price) {
      //return numeral(price).format('0,0').replace(/,/g,'.');
      return numeral(price).format('0,0.00');
  }

  //Cálculo para mostrar la lista de articulos en el detalle con cantidades
  setSubtotalProduct(price, quantity) : number {
      return numeral(price * quantity).format('0,0.00');
  }

  getOffer(price, percent) {
    let result = price * (percent / 100);
    return price - result;
  };

  //Sacar el peso del producto
  getGrams(price, gr) {
    return (price / 1000) * gr;
  }

  //Llevar el cálculo de las opciones que tenga el producto
  setTotalOptions(options) {
    let total = 0;
    let arrayvalue = options.map((a) => a.value);
    for (const item of arrayvalue) {
      total += item;
    }
    //console.log('total opt', total);
    return total;
  }

  setSupTotalProduct(product, quantity, price, options, grams) {
    let priceres = product.offer
      ? this.getOffer(price, product.offerpercent)
      : price;
    if (product.pricebykg) {
      return (
        ((priceres * grams) / 1000 + this.setTotalOptions(options)) * quantity
      );
    } else {
      return (priceres + this.setTotalOptions(options)) * quantity;
    }
  }

  //Mostrar el monto total de los artículos con sus opciones, gramos, y opciones (si tiene)
  getSubTotal(products) {
    let value = 0;
    for (const element of products) {
      value += this.setSupTotalProduct(
        element.product,
        element.quantity,
        element.price,
        element.options,
        element.grams
      );
    }
    return value;
  }

  //Funcion para sacar el calculo de la oferta para el subtotal de carrito
  getOfferSubtotal(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  };

  //Mostrar el subtotal de los productos comprados en el carrito sin envío ni comisiones
  setSubtotalCart() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOfferSubtotal(element.price, element['product'].offerpercent)), element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += option.value * element.quantity;
          }        
      } else {
        sum += this.getOfferSubtotal(element.price, element['product'].offerpercent) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += option.value * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum);
    return sum + sumoption;
  }

}
