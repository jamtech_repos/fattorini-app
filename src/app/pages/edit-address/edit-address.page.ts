import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.page.html',
  styleUrls: ['./edit-address.page.scss'],
})
export class EditAddressPage implements OnInit {

  user : any = [];
  isLoader : boolean = false;
  config : any = [];
  listStateCity : any = [];
  cityArray : any = [];
  address : any = [];
  zones : any = [];
  z : any = [];
  myToast : any;
  stateSelected : any;
  citySelected : any;
  sectorSelected : any;
  dirSelected : any;
  addressSelected : any;

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage, 
    private navCtrl: NavController, private toastCtrl: ToastController, private activatedRoute: ActivatedRoute,
    private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.dirSelected = this.activatedRoute.snapshot.queryParamMap.get('id');
  }

  ionViewWillEnter() {
    this.getUserLogged();
    this.showAppConfig();
  }

  showAppConfig() {
    this.dataservice.getAppConfig()
    .subscribe(res => {
      this.config = res;
      this.zones = res['zones'];
      console.log(this.config, 'config', this.zones, 'zones');
    });
  }

  showAddress(user) {
    this.dataservice.getListUserAddress(user)
    .subscribe(res => {
      this.address = res;
      let a = this.address.filter(item => item._id === this.dirSelected)
      this.stateSelected = a[0].state;
      this.citySelected = a[0].city;
      this.sectorSelected = a[0].sector;
      this.addressSelected = a[0].address;

      this.dataservice.getListCountry()
      .subscribe(result => {
        this.listStateCity = result;
          let c = this.listStateCity.filter(item => item.estado === this.stateSelected);
          this.cityArray = c[0].ciudades;
          console.log(this.cityArray)
      });
    });
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.stateSelected = event.target.value;

        if(this.listStateCity.length > 0) {
          let c = this.listStateCity.filter(item => item.estado === this.stateSelected)
          this.cityArray = c[0].ciudades;
          this.citySelected = c[0].ciudades[0];
        }
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.citySelected = event.target.value;

      this.z = this.zones.filter((a) => a.city === this.citySelected && a.state === this.stateSelected)
      .map((a) => a.sectors)
      .flat();
      let sectorsort = this.z.sort((a,b) => {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        return a < b ? -1 : a > b ? 1 : 0;
      })
      console.log(sectorsort);
    }
  }

  changeSector(event) {
    if(event.target.value && event.target.value != '') {
      this.sectorSelected = event.target.value;
    }
  }

  //Actualizar direcciones
  editAddress(state, city, sector, address) {
    this.dataservice.updateAddressUser(this.dirSelected, state, city, sector, address)
    .subscribe(res => {
      let edit = res;
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "Dirección actualizada con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
    });
  }

  popPage() {
    this.navCtrl.pop();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.showAddress(this.user._id);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

}
