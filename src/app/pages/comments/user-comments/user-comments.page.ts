import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-user-comments',
  templateUrl: './user-comments.page.html',
  styleUrls: ['./user-comments.page.scss'],
})
export class UserCommentsPage implements OnInit {

  user : any = [];
  listComments : any = [];
  isLoader : boolean = false;
  moment: any = moment;

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage, private router: Router,
    private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getUserLogged();
  }

  showComments(user) {
    this.dataservice.getCommentsUser(user)
    .subscribe(res => {
      this.isLoader = true;
      this.listComments = res;
      console.log(this.listComments, 'comentarios');
    });
  }

  addComment(id) {
    this.router.navigate(['/addcomment'], { queryParams: { id:id } });
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.isLoader = true;
          this.user = result;
          this.showComments(result['_id']);
          console.log(this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 
}
