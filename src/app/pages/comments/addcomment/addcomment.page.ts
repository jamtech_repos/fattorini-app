import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';
import * as moment from 'moment';

@Component({
  selector: 'app-addcomment',
  templateUrl: './addcomment.page.html',
  styleUrls: ['./addcomment.page.scss'],
})
export class AddcommentPage implements OnInit {

  contador = 0;
  id : any;
  review : any;
  rate = 0;
  myToast : any;
  hour : string = '';

  constructor(private activatedRoute: ActivatedRoute, private dataservice: DataService, private loadingservice: LoadingService,
              private navCtrl: NavController, private toastCtrl: ToastController, private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
  }

  //Cerrar página
  popPage() {
    this.navCtrl.pop();
  }

  //Score de las estrellas
  onRate(rate) {
    console.log(rate)
    this.rate = rate;
  }

  //Contador de caracteres
  onKey(event){
    this.contador = event.target.value.length;
  }

  //Guardar comentario
  saveComment(score, text) {
    this.loadingservice.loadingPresent();
    this.dataservice.addCommentUser(this.id, score, text, 1)
    .subscribe(res => {
      this.review = res;
      this.loadingservice.loadingDismiss();
      console.log(this.review, 'comentario guardado');
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "Su comentario ha sido agregado con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      this.router.navigate(['/user-comments'], {
        queryParams: { hour: this.hour }, replaceUrl: true 
      });
    });
  }

}
