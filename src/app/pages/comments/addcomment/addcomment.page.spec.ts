import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddcommentPage } from './addcomment.page';

describe('AddcommentPage', () => {
  let component: AddcommentPage;
  let fixture: ComponentFixture<AddcommentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcommentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddcommentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
