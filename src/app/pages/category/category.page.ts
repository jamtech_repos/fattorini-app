import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SERVER_URL } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
  
  isSkeleton : boolean = false;
  category : any[] = [];
  textoBuscar = '';
  quantity : any = [];
  hour: string = '';
  user: any = [];
  currency: string = '';
  dataP : any[] = [];
  page = 0;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, public storage: Storage,
              private http: HttpClient, private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.getCategoriesList(false, "");
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency category', this.currency);
      }
    } ));
    this.getUserLogged();
  }

  getCategoriesList(isFirstLoad, event)
  {
    this.dataservice.getListCategories(this.page)
    .subscribe(result => {
      this.isSkeleton = true;
      this.dataP = result;

      let maximumPage = this.dataP['pages'];

      for (const cat of result['categories']) {
        this.category.push(cat);
      }

      if(this.page < maximumPage || this.page === 0){
        if(isFirstLoad)
          event.target.complete();
          this.page++;
      } else
          this.infiniteScroll.disabled = true;

        console.log(this.category, 'category');
        console.log('page', this.page);
        console.log('max page', this.dataP);
        console.log(`page=${this.page}, max=${maximumPage}`);
      
    })
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  loadMoreCategories(event) {
    this.getCategoriesList(true, event);
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          // Mostrar el total de productos que tiene el usuario en el carrito
          this.dataservice.getTotalCart(this.user._id)
          .subscribe(result => {
            this.quantity = result;
            console.log('total', this.quantity);
          })
          console.log('dataUser', this.user);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  }

}
