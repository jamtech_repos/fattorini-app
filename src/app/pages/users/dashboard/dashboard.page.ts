import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CURRENCY_BS, SERVER_URL } from 'src/environments/environment';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import * as numeral from 'numeral';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public slideOptionsCat = {
    slidesPerView: 3,
    centeredSlides: false,
    pagination: false,
    spaceBetween: -10,
    loop: false,
  };

  public slideOptions = {
    slidesPerView: 2,
    centeredSlides: false,
    pagination: false,
    spaceBetween: -10,
    loop: false,
  };

  // Slide para mostrar las categorías
  slideCat: any[] = [];
  categoryArray: any[] = [];
  // Slide para mostrar los comercios
  slideCommerce: any[] = [];
  // Variable booleana que sirve para mostrar el skeleton de carga
  isSkeleton: boolean = false;
  depth = 0;
  quantity : any = [];
  hour: string = '';
  user: any = [];
  currency: string = '';
  listCommerceCity : any = [];
  listCommerceState : any = [];
  slideHome : any = [];
  slideFilter : any = [];
  popular : any = [];
  promos : any = [];
  infoip: any;
  ip : any;
  url : string;
  _BS = CURRENCY_BS;
  active : any;

  public slideOptionsHome = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '1.2',
    centeredSlides: false,
    loop: false,
    spaceBetween: -20,
  };

  constructor(private auth: AuthenticationService, public alertCtrl: AlertController,
              private dataservice: DataService, public storage: Storage, private iab: InAppBrowser,
              private activatedRoute: ActivatedRoute, private http: HttpClient, private networkInterface: NetworkInterface) { }            

  ngOnInit() {
    // Obtiene por parámetro la hora actual para la validación de los horarios del comercio
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency dashboard', this.currency);
      }
    } ));
    this.getCategories();
    this.getCommerce();
    this.listCommercebyState();
    this.getUserLogged();
    this.getSlide();
    this.getIp();
  }

  openWebpage(url) {
    const option: InAppBrowserOptions = {
      footer: 'no'
    }
    if(url != '') {
      this.iab.create(url, '_system' , option);
    }
  }

  //Mostrar el slider de banners
  getSlide() {
    this.dataservice.getBanners()
    .subscribe(result => {
      this.isSkeleton = true;
      this.slideFilter = result;

      let c  = this.slideFilter.filter(item => item.type === 'app')
      this.slideHome = c;

      console.log(this.slideHome, 'slide filter app', this.slideFilter, 'slide res');
    }); 
  }

  // Actualizar la vista
  doRefresh(event) {
    setTimeout(() => {
      this.getCategories();
      this.getCommerce();
      this.getUserLogged();
      this.listCommercebyState();
      this.getSlide();
      this.getIp();
      event.target.complete();
    }, 2000);
  }

  // Mostrar los comercios mejor calificados
  showBestQualified(city)
  {
    this.dataservice.getPopularCommerce(city, this.hour)
    .subscribe(result => {
      this.popular = result.slice(0, 10);
      console.log(this.popular, 'popular');
    })
  }

  // Listar las categorías en el orden colocado en la administrativa, 
  // muestra solo 8 pero se puede cambiar el valor en la línea 146 de data.service
  getCategories()
  {
    let page = 0;
    this.dataservice.listCategoryHome(page)
    .subscribe(result => {
      this.isSkeleton = true;
      this.categoryArray = result['categories'];
      console.log(this.categoryArray);
    })
  }

  // Listar todos los comercios aprobados
  // muestra solo 5 pero se puede cambiar el valor en la línea 81
  getCommerce()
  {
    this.dataservice.listCommerce(this.hour)
    .subscribe(result => {
      this.isSkeleton = true;
      this.slideCommerce = result.slice(0, 10);
      console.log(this.slideCommerce);
    })
  }

  // Alerta
  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  // Mostrar alerta de confirmación para cerrar sesión
  logout() {
    this.presentAlertConfirm();
  }

  listPromos(city) {
    this.dataservice.listPromo(city)
    .subscribe(res => {
      this.isSkeleton = true;
      this.promos = res.slice(0, 10);
      console.log(this.promos, 'promos');
    })    
  }

  listCommercebyCity(city, hour) {
    this.dataservice.getCommercebyCity(city, hour)
    .subscribe(res => {
      this.isSkeleton = true;
      this.listCommerceCity = res.slice(0, 10);
      console.log(this.listCommerceCity);
    })    
  }

  getIp() {
    this.networkInterface.getWiFiIPAddress()
    .then(address => console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`))
    .catch(error => console.error(`Unable to get IP: ${error}`));

    this.networkInterface.getCarrierIPAddress()
      .then(address => console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`))
      .catch(error => console.error(`Unable to get IP: ${error}`));
  }

  listCommercebyState() {
    this.dataservice.getInfoIp()
    .subscribe(res => {
      this.ip = res;
      let state = res['region'];
      this.dataservice.getCommerceByIp(state, this.hour)
      .subscribe(res => {
        this.isSkeleton = true;
        this.listCommerceState = res.slice(0, 10);
        console.log(this.listCommerceState, 'lista de comercios por estado');
      })
    });  
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    //return numeral(price).format('0,0').replace(/,/g,'.');
    return numeral(price).format('0,0.00');
  }

  async userActiveLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Su usuario se encuentra deshabilitado',
      message: 'Ha sido dado de baja o a desactivado su cuenta, contacte con soporte',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            console.log('Yes clicked');
            this.auth.logout();
          }
        }
      ],
      backdropDismiss: false,
    });
      await alert.present();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.showBestQualified(this.user.city);
          this.listCommercebyCity(this.user.city, this.hour);
          this.listPromos(this.user.city);
          // Mostrar el total de productos que tiene el usuario en el carrito
          this.dataservice.getTotalCart(this.user._id)
          .subscribe(result => {
            this.quantity = result;
            console.log('total', this.quantity);
          })
          console.log('dataUser', this.user);
          // ¿Usuario activo?
          this.dataservice.getUserActive(this.user._id)
          .subscribe(result => {
            this.active = result;

            if(this.active.status === false) {
              this.userActiveLogout();
            }
            console.log('active user', this.active.status);
          })
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });
    }
  }
  
}
