import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.page.html',
  styleUrls: ['./reviews.page.scss'],
})
export class ReviewsPage implements OnInit {

  id : any;
  listComments : any = [];
  firstname : string = '';
  isLoader : boolean = false;
  moment: any = moment;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, private loadingservice: LoadingService,
    private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.queryParamMap.get('id');
  }

  ionViewWillEnter() {
    this.showComments();
  }

  getCountComments() {
    let count = 0;
    count = this.listComments.length;
    return count;
  }

  showComments() {
    //this.loadingservice.loadingPresent();
    this.dataservice.getCommentsCommerce(this.id)
    .subscribe(res => {
      this.isLoader = true;
      this.listComments = res;
      //this.loadingservice.loadingDismiss();
      console.log(this.listComments, 'reseñas comercio');
    },
    async (error: HttpErrorResponse) => {
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    }); 
  }

  getFirstname(string) {
    return string.charAt(0);
  }

}
