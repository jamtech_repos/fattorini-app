import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-filtermenu',
  templateUrl: './filtermenu.page.html',
  styleUrls: ['./filtermenu.page.scss'],
})
export class FiltermenuPage implements OnInit {

  commerce : any;
  menu : any[] = [];
  isLoader : boolean = false;
  textoBuscar = '';
  currency : any;
  user : any;

  constructor(private activatedRoute : ActivatedRoute, private dataservice : DataService, private router : Router,
    private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.commerce = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.listMenuCommerce();
  }

  listMenuCommerce() {
    this.dataservice.listMenuCommerce(this.commerce)
    .subscribe(result => {
      this.isLoader = true;
      this.menu = result;
      console.log(this.menu, 'menu');
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });  
  }

  filtrarmenu(idMenu) {
    this.router.navigate(['/productsmenu'], { queryParams: { menu:idMenu, currency:this.currency, user:this.user, commerce:this.commerce} }); 
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

}
