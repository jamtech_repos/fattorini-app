import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FiltermenuPage } from './filtermenu.page';

describe('FiltermenuPage', () => {
  let component: FiltermenuPage;
  let fixture: ComponentFixture<FiltermenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltermenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FiltermenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
