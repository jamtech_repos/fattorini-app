import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { CURRENCY_BS, SERVER_URL } from 'src/environments/environment';
import * as numeral from 'numeral';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
  
  isSkeleton : boolean = false;
  search : any[] = [];
  products : any[] = [];
  commerce : any[] = [];
  category : any[] = [];
  menus : any[] = [];
  textoBuscar = '';
  quantity : any = [];
  hour: string = '';
  user: any = [];
  currency: string = '';
  idUser : any;
  _BS = CURRENCY_BS;
  active : any;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, public storage: Storage,
              private http: HttpClient, private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency category', this.currency);
      }
    } ));
    this.getUserLogged();
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
    this.dataservice.search(this.textoBuscar)
    .subscribe(res => {
      this.search = res;
      this.products = res['products'];
      this.commerce = res['commerces'];
      this.category = res['categories'];
      this.menus = res['productsByMenu'];
      console.log(this.search, 'search');
    });
  }

  getCountProducts() {
    let count = 0;
    count = this.products.length;
    return count;
  }

  getCountCommerce() {
    let count = 0;
    count = this.commerce.length;
    return count;
  }

  getCountCategory() {
    let count = 0;
    count = this.category.length;
    return count;
  }

  getCountProductsByMenu() {
    let count = 0;
    count = this.menus.length;
    return count;
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  async userActiveLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Su usuario se encuentra deshabilitado',
      message: 'Ha sido dado de baja o a desactivado su cuenta, contacte con soporte',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            console.log('Yes clicked');
            this.auth.logout();
          }
        }
      ],
      backdropDismiss: false,
    });
      await alert.present();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.idUser = this.user._id;
          // Mostrar el total de productos que tiene el usuario en el carrito
          this.dataservice.getTotalCart(this.user._id)
          .subscribe(result => {
            this.quantity = result;
            console.log('total', this.quantity);
          })
          console.log('dataUser', this.user);
          // ¿Usuario activo?
          this.dataservice.getUserActive(this.user._id)
          .subscribe(result => {
            this.active = result;

            if(this.active.status === false) {
              this.userActiveLogout();
            }
            console.log('active user', this.active.status);
          })
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  }

}
