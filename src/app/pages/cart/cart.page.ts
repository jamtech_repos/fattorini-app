import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as numeral from 'numeral';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';
import { CURRENCY_BS } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  products : any[] = [];
  user: string = '';
  hour: string = '';
  quantity : any = [];
  refresh: any;
  subtotal : number;
  currency : string = '';
  isLoader : boolean = false;
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute,
              public storage: Storage, private router: Router, private alertCtrl : AlertController,
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
    console.log('init cart');
  }

  ionViewWillEnter() {
    this.listProductCart();
    this.getQuantityCart();
    console.log('ionViewWillEnter cart');
  }

  ionViewWillLeave() {
    console.log('leaving cart');
  }

  createOrder() {
    if(this.user) {
      this.dataservice.createOrderUser(this.user, this.hour)
      .subscribe(result => {
        let order = result;
        this.router.navigate(['/orderspanel'], { queryParams: { hour: this.hour, currency: this.currency } });
        console.log(order, 'order');
      },
      async (error: HttpErrorResponse) => {
        //console.log(error)
         if(error.status === 401) {
          const alert = await this.alertCtrl.create({
            header: 'Stock insuficiente',
            message: error.error.error,
            buttons: ['OK']
          });
          await alert.present();
        }
        else if(error.status === 403) {
          const alert = await this.alertCtrl.create({
            header: 'Comercio cerrado',
            message: error.error.error,
            buttons: ['OK']
          });
          await alert.present();
        }
        else if(error.status === 402) {
          const alert = await this.alertCtrl.create({
            header: 'Producto agotado',
            message: error.error.error,
            buttons: ['OK']
          });
          await alert.present();
        }
        console.log(error);
      });
    }
  }

  listProductCart() {
    this.dataservice.getProductCart(this.user, this.currency)
    .subscribe(result => {
      this.isLoader = true;
      this.products = result;
      console.log(this.products, 'products');
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  //Funcion para sacar el calculo de la oferta para el subtotal de carrito
  getOfferSubtotal(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  };

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  };

  getGrams(price, gr) {
    return (price / 1000) * gr;
  }

   setSubtotalCart() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOffer(element.price, element['product'].offerpercent)), element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += option.value * element.quantity;
          }        
      } else {
        sum += this.getOffer(element.price, element['product'].offerpercent) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += option.value * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum);
    return sum + sumoption;
  }

  setSubtotalCartBs() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOffer(element.price, element['product'].offerpercent)) * element['product'].exchange_rate, element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price * element['product'].exchange_rate), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += (option.value * element['product'].exchange_rate) * element.quantity;
          }        
      } else {
        sum += (this.getOffer(element.price, element['product'].offerpercent) * element['product'].exchange_rate) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += (option.value * element['product'].exchange_rate) * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum);
    return sum + sumoption;
  }

  setTotalOptions(options) {
    let total = 0;
    let arrayvalue = options.map((a) => a.value);
    for (const item of arrayvalue) {
      total += item;
    }
    //console.log('total opt', total);
    return total;
  }

  doRefresh(event) {
    setTimeout(() => {
      this.listProductCart();
      this.getQuantityCart();
      event.target.complete();
    }, 2000);
  }

  // Mostrar la cantidad total de productos agregados al carrito
  getQuantityCart() {
    this.dataservice.getTotalCart(this.user)
    .subscribe(result => {
      this.quantity = result;
      console.log('total', this.quantity);
    })
  }

  setSubtotalProduct(price, quantity) : number {
    return numeral(price * quantity).format('0,0.00');
  }

  setSubtotalProductBs(price, quantity) : number {
    //return numeral(price * quantity).format('0,0').replace(/,/g,'.');
    return numeral(price * quantity).format('0,0.00');
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    //return numeral(price).format('0,0').replace(/,/g,'.');
    return numeral(price).format('0,0.00');
  }

  addProduct(product, quantity) {
    this.dataservice.updateProductCart(product, quantity)
    .subscribe(result => {
      let res = result;
      console.log('add', res);

      this.dataservice.getTotalCart(this.user)
      .subscribe(result => {
        let t = result;
        console.log('total cart add', t);
      })
      this.listProductCart();
      this.getQuantityCart(); 
    })  
  }

  removeProduct(product, quantity) {
    if(quantity > 0) {
      this.dataservice.updateProductCart(product, quantity)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);

        this.dataservice.getTotalCart(this.user)
        .subscribe(result => {
          let t = result;
          console.log('total cart rm', t);
        })
        this.listProductCart();
        this.getQuantityCart();
      })  
    }
  }

  deleteProductCart(id) {
    this.dataservice.removeProductCart(id)
    .subscribe(result => {
      let res = result;
      console.log('delete', res);

      this.listProductCart();
      this.getQuantityCart(); 
    })
  }

}
