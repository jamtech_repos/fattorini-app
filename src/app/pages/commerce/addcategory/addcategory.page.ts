import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.page.html',
  styleUrls: ['./addcategory.page.scss'],
})
export class AddcategoryPage implements OnInit {

  config : any = [];
  commerce : any = [];
  menu : any = [];
  user : any = [];
  isLoader : boolean = false;
  myToast : any;
  add = '';

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage, private navCtrl: NavController,
    private toastCtrl: ToastController, private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getUserLogged();
  }

  showConfigCommerce(id) {
    this.dataservice.getConfigCommerce(id)
    .subscribe(result => {
      this.config = result;
      console.log('config', this.config);
    });
  }

  popPage() {
    this.navCtrl.pop();
  }

  addMenu(name) {
    this.dataservice.createMenu(name, 0, this.menu.length, this.config['commerce'])
    .subscribe(res => {
      this.menu = res;
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: name + " ha sido creada con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      console.log(this.menu);
    })    
  }

  buscarInfoCommerce(user) {
    this.dataservice.buscarInfoCommercebyUser(user)
    .subscribe(res => {
      this.commerce = res['commerce'];
      this.showConfigCommerce(res['commerce']._id);
        this.dataservice.listMenuCommerce(res['commerce']._id)
        .subscribe(result => {
          this.menu = result;
          console.log('menu', this.menu.length);
        });
      console.log(this.commerce);
    })
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.isLoader = true;
          this.buscarInfoCommerce(this.user._id);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

}
