import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersDetailDashboardPageRoutingModule } from './orders-detail-dashboard-routing.module';

import { OrdersDetailDashboardPage } from './orders-detail-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersDetailDashboardPageRoutingModule
  ],
  declarations: [OrdersDetailDashboardPage]
})
export class OrdersDetailDashboardPageModule {}
