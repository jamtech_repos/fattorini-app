import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersDetailDashboardPage } from './orders-detail-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersDetailDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersDetailDashboardPageRoutingModule {}
