import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';
import * as numeral from 'numeral';
import * as moment from 'moment';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { CURRENCY_BS } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-orders-detail-dashboard',
  templateUrl: './orders-detail-dashboard.page.html',
  styleUrls: ['./orders-detail-dashboard.page.scss'],
})
export class OrdersDetailDashboardPage implements OnInit {

  currency : string = '';
  order : string = '';
  detail : any = [];
  products : any = [];
  commerce : any = [];
  config : any = [];
  detailpayment : any = [];
  bankdata : any = [];
  isLoader : boolean = false;
  idCA : string = '';
  options : any = [];
  deliveryman : any = [];
  selectedDeliveryMan : any = [];
  myToast : any;
  moment: any = moment;
  _BS = CURRENCY_BS;

  constructor(private dataservice: DataService, private toastCtrl: ToastController, private downloader: Downloader,
    private activatedRoute: ActivatedRoute, private loadingService: LoadingService, private photoViewer: PhotoViewer,
    private auth: AuthenticationService, public alertCtrl: AlertController) { }

  ngOnInit() {
    this.order = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.idCA = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.showOrderDetail();
    console.log(this.idCA, 'id commerce');
  }

  downloadFile(url) {
    var request: DownloadRequest = {
      uri: url,
      title: 'voucher-' + Math.random().toString(36).substring(7),
      description: '',
      mimeType: 'image/jpeg',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
          dirType: 'Downloads',
          subPath: 'MyFile.jpeg'
      }
    };

    this.loadingService.loadingPresent();
    this.downloader.download(request)
      .then((location: string) =>
      this.myToast = this.toastCtrl.create({
        message: 'El comprobante de pago fue descargado con éxito',
        duration: 2000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(location);
        toastData.present();
        this.loadingService.loadingDismiss();
      }))
      .catch((error: any) => console.error(error));
  }

  showImg(url) {
    this.photoViewer.show(url);
  }

  showOrderDetail() {
    this.loadingService.loadingOrder();
    this.dataservice.getOrderDetails(this.order, 'USD')
    .subscribe(res => {
      this.detail = res;
      this.commerce = this.detail['commerce'];
      this.products = this.detail['products'];
      const de = res['delivery_man'];

      //consultar el repartidor y asociar el ID con su nombre de pila
      this.dataservice.getDeliveryman()
      .subscribe(res => {
        this.deliveryman = res;
        console.log(this.deliveryman, 'deliveryman')

        //iterar las ordenes para sacar el id del repartidor
        for (let i = 0; i < this.deliveryman.length; i++) {
          const element = this.deliveryman[i]._id;
          console.log(element, de, 'ver aqui')
          //comparar ids de ordenes y repartidores
          if(element === de) {
            this.selectedDeliveryMan = this.deliveryman[i].user.firstname + ' ' + this.deliveryman[i].user.lastname;
          }
        }
      });

      this.dataservice.getOrderPaymentDetail(this.order, this.currency)
      .subscribe(res => {
        this.detailpayment = res;
        if(this.detail.status >= 2) {
          this.bankdata = this.detailpayment['bankdata'];
        }
        console.log('detail payment', this.detailpayment, this.bankdata, 'bankdata');
      });
      this.loadingService.loadingDismissOrder();
      console.log('order detail', this.detail);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  // Mostrar formato $ / Bs.S
  priceFormat(price) {
    if(this.currency === 'USD') {
      return numeral(price).format('0,0.00');
    } 
    else if(this.currency === 'Bs.S') {
      //return numeral(price).format('0,0').replace(/,/g,'.');
      return numeral(price).format('0,0.00');
    }
  }

  priceFormatProduct(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
      //return numeral(price).format('0,0').replace(/,/g,'.');
      return numeral(price).format('0,0.00');
  }

  setSubtotalProduct(price, quantity) : number {
      return numeral(price * quantity).format('0,0.00');
  }

  getOffer(price, percent) {
    let result = price * (percent / 100);
    return price - result;
  };

  getGrams(price, gr) {
    return (price / 1000) * gr;
  }

  setTotalOptions(options) {
    let total = 0;
    let arrayvalue = options.map((a) => a.value);
    for (const item of arrayvalue) {
      total += item;
    }
    //console.log('total opt', total);
    return total;
  }

  setSupTotalProduct(product, quantity, price, options, grams) {
    let priceres = product.offer
      ? this.getOffer(price, product.offerpercent)
      : price;
    if (product.pricebykg) {
      return (
        ((priceres * grams) / 1000 + this.setTotalOptions(options)) * quantity
      );
    } else {
      return (priceres + this.setTotalOptions(options)) * quantity;
    }
  }

  getSubTotal(products) {
    let value = 0;
    for (const element of products) {
      value += this.setSupTotalProduct(
        element.product,
        element.quantity,
        element.price,
        element.options,
        element.grams
      );
    }
    return value;
  }

  getOfferSubtotal(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  };

  setSubtotalCart() {
    let sum = 0;
    let sumoption = 0;

    for (let i = 0; i < this.products.length; i++) {
      const element = this.products[i];
      
      if(element['product'].pricebykg) {
        if(element['product'].offer) {
          sum += this.getGrams((this.getOfferSubtotal(element.price, element['product'].offerpercent)), element.grams) * element.quantity; 
        } else
          sum += this.getGrams((element.price), element.grams) * element.quantity;
          for (let j = 0; j < element.options.length; j++) {
            const option = element.options[j];
            sumoption += option.value * element.quantity;
          }        
      } else {
        sum += this.getOfferSubtotal(element.price, element['product'].offerpercent) * element.quantity;
        for (let j = 0; j < element.options.length; j++) {
          const option = element.options[j];
          sumoption += option.value * element.quantity;
        }         
      }

    }
    //console.log('subtotal', sum);
    return sum + sumoption;
  }

}
