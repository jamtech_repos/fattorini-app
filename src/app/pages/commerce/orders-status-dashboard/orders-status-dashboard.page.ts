import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-orders-status-dashboard',
  templateUrl: './orders-status-dashboard.page.html',
  styleUrls: ['./orders-status-dashboard.page.scss'],
})
export class OrdersStatusDashboardPage implements OnInit {

  id : string = '';
  order : any = [];
  status : any = [];
  selectedValStatus : any;
  isLoader : boolean = false;

  data = {
    status : ''
  }

  listStatus: any[] = [
/*     {
      id: 0,
      name: 'En espera',
    },
    {
      id: 1,
      name: 'Pendiente de pago',
    }, */
    {
      id: 2,
      name: 'Pago realizado',
    },
    {
      id: 3,
      name: 'Pago confirmado',
    },
    {
      id: 4,
      name: 'Pedido en preparación',
    },
    {
      id: 5,
      name: 'Listo para ser enviado',
    },
    {
      id: 8,
      name: 'Pedido en camino',
    },
    {
      id: 9,
      name: 'Pedido entregado',
    },
    {
      id: 10,
      name: 'Cancelado',
    }
  ];

constructor(public router: Router, public activatedRoute: ActivatedRoute, private dataservice: DataService,
  private navCtrl: NavController, private loadingService: LoadingService, private auth: AuthenticationService, public alertCtrl: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.queryParamMap.get('order');
    console.log('init status');
  }

  close() {
    this.navCtrl.pop();
  }

  ionViewWillEnter() {
    this.getOrder();
    console.log('ionViewWillEnter orders-status-dashboard');
  }

  getOrder() {
    this.loadingService.loadingPresent();
    this.dataservice.getOrderDetails(this.id, 'USD')
    .subscribe(res => {
      this.order = res;
      this.isLoader = true;
      this.data.status = res['status'];

      for (let i = 0; i < this.listStatus.length; i++) {
        const element = this.listStatus[i];
        if(element.id === this.data.status) {
          if(element.id >= 3) {
            this.listStatus.splice(0, 1);
            this.selectedValStatus = element.name;
            console.log(this.listStatus, 'status arr');
          } else {
            this.selectedValStatus = element.name;
          }
        }
      }
      this.loadingService.loadingDismiss();
      console.log('order detail', this.order, this.selectedValStatus, 'status');
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  //Cambiar estado
  changeStatus(event) {
    if(event.target.value && event.target.value != '') {
      for (let i = 0; i < this.listStatus.length; i++) {
        const element = this.listStatus[i].id;
        if(element === event.target.value) {
          this.selectedValStatus = this.listStatus[i].name;
          console.log(event.target.value);
        }
      } 
    }
  }

  updateStatus(id) {
    if(parseInt(this.data.status) === 2) {
      this.dataservice.changeStatusOrder(id, 2)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else
     if(parseInt(this.data.status) === 3) {
      this.dataservice.changeStatusOrder(id, 3)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else
    if(parseInt(this.data.status) === 4) {
      this.dataservice.changeStatusOrder(id, 4)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else 
    if(parseInt(this.data.status) === 5) {
      this.dataservice.changeStatusOrder(id, 5)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else 
    if(parseInt(this.data.status) === 8) {
      this.dataservice.changeStatusOrder(id, 8)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else 
    if(parseInt(this.data.status) === 9) {
      this.dataservice.changeStatusOrder(id, 9)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    } else 
    if(parseInt(this.data.status) === 10) {
      this.dataservice.changeStatusOrder(id, 10)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.close();
      })
    }
    console.log(this.data.status, 'data status')
  }

}
