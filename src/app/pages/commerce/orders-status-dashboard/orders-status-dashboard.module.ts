import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersStatusDashboardPageRoutingModule } from './orders-status-dashboard-routing.module';

import { OrdersStatusDashboardPage } from './orders-status-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersStatusDashboardPageRoutingModule
  ],
  declarations: [OrdersStatusDashboardPage]
})
export class OrdersStatusDashboardPageModule {}
