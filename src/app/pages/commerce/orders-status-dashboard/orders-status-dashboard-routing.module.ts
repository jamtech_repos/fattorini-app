import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersStatusDashboardPage } from './orders-status-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersStatusDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersStatusDashboardPageRoutingModule {}
