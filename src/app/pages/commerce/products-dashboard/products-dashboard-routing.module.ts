import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsDashboardPage } from './products-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsDashboardPageRoutingModule {}
