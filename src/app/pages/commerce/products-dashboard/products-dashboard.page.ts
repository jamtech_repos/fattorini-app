import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonInfiniteScroll, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import * as numeral from 'numeral';
import { CURRENCY_BS } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-products-dashboard',
  templateUrl: './products-dashboard.page.html',
  styleUrls: ['./products-dashboard.page.scss'],
})
export class ProductsDashboardPage implements OnInit {

  isSkeleton: boolean = false;
  hour: string = '';
  currency: string = '';
  page = 0;
  dataP : any = [];
  products : any = [];
  user : any = [];
  commerce : any = [];
  idCommerce : any = [];
  textoBuscar = '';
  isStock : any;
  myToast : any;
  _BS = CURRENCY_BS;

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, private toastCtrl: ToastController,
    private auth: AuthenticationService, public alertCtrl: AlertController) { }

  ngOnInit() {
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.idCommerce = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.getProductCommerce(false, "");
  }

  ionViewWillEnter() {
  }

  // Mostrar formato $ / Bs.S
  priceFormat(price) {
    if(this.currency === 'USD') {
      return numeral(price).format('0,0.00');
    } 
    else if(this.currency === 'Bs.S') {
      //return numeral(price).format('0,0').replace(/,/g,'.');
      return numeral(price).format('0,0.00');
    }
  }

  //Cambiar status del producto Disponible / Agotado
  changeStock(id, event) {
    this.dataservice.updateStockStatus(id, event)
     .subscribe(result => {
       this.isStock = result;
        this.myToast = this.toastCtrl.create({
        message: "El status del producto ha sido actualizado",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      console.log(this.isStock, 'isStock');
     });
  }

  getProductCommerce(isFirstLoad, event) {
    this.dataservice.listProductCommerce(this.idCommerce, this.page, this.hour, this.currency)
      .subscribe(result => {
        this.isSkeleton = true;
        this.dataP = result;
        console.log('res', this.dataP);
    
        let maximumPage = this.dataP['pages'];

        for (const product of result['products']) {
          this.products.push(product);
        }        

        if(this.page < maximumPage || this.page === 0){
          if(isFirstLoad)
            event.target.complete();
            this.page++;
        } else
            this.infiniteScroll.disabled = true;

      console.log(this.products, 'products');
      console.log('page', this.page);
      console.log('max page', this.dataP);
      console.log(`page=${this.page}, max=${maximumPage}`);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  loadMoreProducts(event) {
    this.getProductCommerce(true, event);
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

}
