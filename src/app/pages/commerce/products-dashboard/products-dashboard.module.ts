import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsDashboardPageRoutingModule } from './products-dashboard-routing.module';

import { ProductsDashboardPage } from './products-dashboard.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsDashboardPageRoutingModule,
    PipesModule
  ],
  declarations: [ProductsDashboardPage]
})
export class ProductsDashboardPageModule {}
