import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryDashboardPageRoutingModule } from './category-dashboard-routing.module';

import { CategoryDashboardPage } from './category-dashboard.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryDashboardPageRoutingModule,
    PipesModule
  ],
  declarations: [CategoryDashboardPage]
})
export class CategoryDashboardPageModule {}
