import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-category-dashboard',
  templateUrl: './category-dashboard.page.html',
  styleUrls: ['./category-dashboard.page.scss'],
})
export class CategoryDashboardPage implements OnInit {

  menu : any = [];
  commerce : any = [];
  user : any = [];
  isLoader : boolean = false;
  textoBuscar = '';
  myToast: any;

  constructor(private dataservice: DataService, private http: HttpClient, private storage: Storage, 
    public alertCtrl: AlertController, private toastCtrl: ToastController, private auth: AuthenticationService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getUserLogged();
  }

  buscarInfoCommerce(user) {
    this.dataservice.buscarInfoCommercebyUser(user)
    .subscribe(res => {
      this.commerce = res['commerce'];
        this.dataservice.listMenuCommerce(res['commerce']._id)
        .subscribe(result => {
          this.isLoader = true;
          this.menu = result;
          console.log('menu', this.menu);
        });
      console.log(this.commerce);
    })
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  //Eliminar menú de la lista
  statusMenu(id, status) {
    this.presentAlertConfirm(id, status);
  }

  updateMenu(id, status) {
    if(status) {
      this.dataservice.changeMenu(id, false)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.ionViewWillEnter();
      })
    } else {
      this.dataservice.changeMenu(id, true)
      .subscribe(result => {
        let res = result;
        console.log('rm', res);
        this.ionViewWillEnter();
      })     
    }
  }

  async presentAlertConfirm(id, status) {
    if(status) {
      const alert = await this.alertCtrl.create({
        header: 'Confirmación',
        message: '¿Desea desactivar este menú?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Si',
            handler: () => {
              console.log('Confirm Okay');
              this.updateMenu(id, status);
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.updateMenu(id, status);
      this.myToast = this.toastCtrl.create({
        message: "El menú ha sido activado con éxito",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
    }
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.buscarInfoCommerce(this.user._id);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

}
