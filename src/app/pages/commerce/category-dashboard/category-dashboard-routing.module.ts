import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryDashboardPage } from './category-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryDashboardPageRoutingModule {}
