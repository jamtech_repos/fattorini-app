import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CURRENCY_BS, SERVER_URL } from 'src/environments/environment';
import { DataService } from 'src/app/services/data.service';
import { PopoverPage } from '../../popover/popover.page';
import { LoadingService } from 'src/app/services/loading.service';
import * as numeral from 'numeral';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  hour : string = '';
  currency: string = '';
  user: any = [];
  quantity : any = [];
  commerce : any = [];
  category : any = [];
  config : any = [];
  orders : any = [];
  data : any = [];
  products : any = [];
  isLoader : boolean = false;
  pending : any = [];
  success : any = [];
  cancel : any = [];
  control : any = [];
  _BS = CURRENCY_BS;
  active : any;

  constructor(private auth: AuthenticationService, public alertCtrl: AlertController, private activatedRoute: ActivatedRoute,
              public storage: Storage, private http: HttpClient, private dataservice: DataService, 
              private popoverCtrl: PopoverController, private loadingService: LoadingService) { }

  ngOnInit() {
    this.hour = this.activatedRoute.snapshot.paramMap.get('hour');
  }

  async presentPopover(event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event : event,
      mode: 'ios'
    });
    await popover.present();
    const { data } = await popover.onWillDismiss();
    console.log('item', data);
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency dashboard CA', this.currency);
      }
    } ));

    this.getUserLogged();
  }

  listOrders(commerce) {
    this.dataservice.getOrderCommerce(commerce, 'USD')
    .subscribe(res => {
      this.isLoader = true;
      this.orders = res;

      for (let i = 0; i < this.orders.length; i++) {
        const element = this.orders[i].status;
        if(element === 0) {
          this.pending.push(element);
          console.log(this.pending, 'pending');
        }
        if(element === 9) {
          this.success.push(element);
          console.log(this.success, 'success');
        }
        if(element === 10) {
          this.cancel.push(element);
          console.log(this.cancel, 'cancel');
        }
      }
      console.log(this.orders, 'orders');
    });
  }

  getProductsCommerce(id) {
    //this.loadingService.loadingPresent();
    this.dataservice.listInfoCommerceComplete(id, this.hour, this.currency)
    .subscribe(result => {
      this.data = result;
      this.products = result['products'];
      //this.loadingService.loadingDismiss();
      console.log(this.data, 'info');
    })
  }

  showConfigCommerce(id) {
    this.dataservice.getConfigCommerce(id)
    .subscribe(result => {
      this.config = result;
      console.log('config', this.config);
    });
  }

  convertDecimal(price) {
    const noTruncarDecimales = {maximumFractionDigits: 20};
    return parseFloat(price).toLocaleString('es', noTruncarDecimales);
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  buscarInfoCommerce(user) {
    this.dataservice.buscarInfoCommercebyUser(user)
    .subscribe(res => {
      this.commerce = res['commerce'];
      this.category = res['commerce'].category;
      this.getProductsCommerce(res['commerce']._id);
      this.showConfigCommerce(res['commerce']._id);
      this.listOrders(res['commerce']._id);
      this.pending = [];
      this.success = [];
      this.cancel = [];
      console.log(this.commerce);
    })
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

  async userActiveLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Su usuario se encuentra deshabilitado',
      message: 'Ha sido dado de baja o a desactivado su cuenta, contacte con soporte',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            console.log('Yes clicked');
            this.auth.logout();
          }
        }
      ],
      backdropDismiss: false,
    });
      await alert.present();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.isLoader = true;
          this.buscarInfoCommerce(this.user._id);
          // ¿Usuario activo?
          this.dataservice.getUserActive(this.user._id)
          .subscribe(result => {
            this.active = result;

            if(this.active.status === false) {
              this.userActiveLogout();
            }
            console.log('active user', this.active.status);
          })
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

}
