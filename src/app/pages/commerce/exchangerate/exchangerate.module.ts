import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExchangeratePageRoutingModule } from './exchangerate-routing.module';

import { ExchangeratePage } from './exchangerate.page';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExchangeratePageRoutingModule,
    NgxMaskIonicModule
  ],
  declarations: [ExchangeratePage]
})
export class ExchangeratePageModule {}
