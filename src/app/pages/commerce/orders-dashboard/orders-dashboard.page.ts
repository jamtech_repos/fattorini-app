import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import * as numeral from 'numeral';
import * as moment from 'moment';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-orders-dashboard',
  templateUrl: './orders-dashboard.page.html',
  styleUrls: ['./orders-dashboard.page.scss'],
})
export class OrdersDashboardPage implements OnInit {

  textoBuscar = '';
  currency = '';
  idCommerce = '';
  orders : any = [];
  isLoader : boolean = false;
  detailpayment : any = [];
  moment: any = moment;  
  deliveryman : any = [];
  selectedDeliveryMan : any = [];

  constructor(private dataservice: DataService, private auth: AuthenticationService, 
    public alertCtrl: AlertController, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.currency = this.activatedRoute.snapshot.queryParamMap.get('currency');
    this.idCommerce = this.activatedRoute.snapshot.queryParamMap.get('id');
  }

  ionViewWillEnter() {
    this.listOrders();
    console.log('ionViewWillEnter orders-dashboard');
  }

  // Actualizar la vista
  doRefresh(event) {
    setTimeout(() => {
      this.ionViewWillEnter();
      event.target.complete();
    }, 2000);
  }

  listOrders() {
    this.dataservice.getOrderCommerce(this.idCommerce, 'USD')
    .subscribe(res => {
      this.isLoader = true;
      this.orders = res;

      //consultar el repartidor y asociar el ID con su nombre de pila
      this.dataservice.getDeliveryman()
      .subscribe(res => {
        this.deliveryman = res;
        console.log(this.deliveryman, 'deliveryman')

        //iterar las ordenes para sacar el id del repartidor
        for (let i = 0; i < this.orders.length; i++) {
          const element1 = this.orders[i].delivery_man;
          //iterar el array de los repartidores para sacar los id
          for (let j = 0; j < this.deliveryman.length; j++) {
            const element2 = this.deliveryman[j]._id;
          //comparar ids de ordenes y repartidores
          if(element1 === element2) {
            this.selectedDeliveryMan = this.deliveryman[j].user.firstname + ' ' + this.deliveryman[j].user.lastname;
          }
        }}
      });    
      console.log(this.orders, 'orders', this.selectedDeliveryMan, 'selectdeliveryman');
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  //Cancelar pedido
  cancelOrder(id) {
    this.presentAlertConfirm(id);
  }

  removeOrder(id) {
    this.dataservice.changeStatusOrder(id, 10)
    .subscribe(result => {
      let res = result;
      console.log('rm', res);
      this.ionViewWillEnter();
    })    
  }

  async presentAlertConfirm(id) {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cancelar este pedido?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.removeOrder(id);
          }
        }
      ]
    });
    await alert.present();
  }

  sumQty(orders) {
    let qty = 0;
    for (let i = 0; i < orders.length; i++) {
       qty += orders[i].quantity;
    }
    return qty;
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
}

getOffer(percent, price) {
  let result = price * (percent / 100);
  return price - result;
};

setTotalOptions(options) {
  let total = 0;
  let arrayvalue = options.map((a) => a.value);
  for (const item of arrayvalue) {
    total += item;
  }
  return total;
}

setSupTotalProduct(product, quantity, price, options, grams) {
  let priceres = product.offer
    ? this.getOffer(product.offerpercent, price)
    : price;
  if (product.pricebykg) {
    return (
      ((priceres * grams) / 1000 + this.setTotalOptions(options)) * quantity
    );
  } else {
    return (priceres + this.setTotalOptions(options)) * quantity;
  }
}

getSubTotal(products) {
  let value = 0;
  for (const element of products) {
    value += this.setSupTotalProduct(
      element.product,
      element.quantity,
      element.price,
      element.options,
      element.grams
    );
  }
  return value;
}

getDetailPayment(order) {
  this.dataservice.getOrderPaymentDetail(order, 'USD')
  .subscribe(res => {
    this.detailpayment = res;
    console.log('detail payment', this.detailpayment);
  });
}

}
