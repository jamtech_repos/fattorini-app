import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersDashboardPageRoutingModule } from './orders-dashboard-routing.module';

import { OrdersDashboardPage } from './orders-dashboard.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersDashboardPageRoutingModule,
    PipesModule
  ],
  declarations: [OrdersDashboardPage]
})
export class OrdersDashboardPageModule {}
