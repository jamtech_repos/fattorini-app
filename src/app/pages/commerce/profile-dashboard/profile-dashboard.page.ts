import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { ActivatedRoute } from '@angular/router';
import { SERVER_URL } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-profile-dashboard',
  templateUrl: './profile-dashboard.page.html',
  styleUrls: ['./profile-dashboard.page.scss'],
})
export class ProfileDashboardPage implements OnInit {

  listStateCity : any = [];
  cityArray : any = [];
  categorySelected : any;
  stateSelected : any;
  citySelected : any;
  category : any = [];
  user : any = [];
  idUser : any;
  commerce : any;
  isImage : boolean = false;
  croppedImagePath = '';
  picture : string = '';
  newPicture : any = [];
  myToast : any;
  dataC : any = [];

  data = {
    category : ''
  }

  constructor(private dataservice: DataService, private loadingService: LoadingService, private storage: Storage,
              private camera : Camera, private crop: Crop, private navCtrl: NavController, private http: HttpClient,
              private file: File, private toastCtrl: ToastController, private activatedRoute: ActivatedRoute,
              private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getUserLogged();
    this.getCategoriesList();
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
          this.idUser = this.user._id;
          this.buscarInfoCommerce(this.idUser);
          this.getListStateCity(this.idUser);
          console.log('id', this.idUser);
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

  buscarInfoCommerce(user) {
    this.dataservice.buscarInfoCommercebyUser(user)
    .subscribe(res => {
      this.dataC = res['commerce'];
      this.commerce = res['commerce']._id;
      this.data.category = res['commerce'].category? res['commerce'].category._id:' ';
      console.log(this.commerce, 'dataC', this.dataC);
    })
  }

  getCategoriesList()
  {
    this.loadingService.loadingPresent();
    this.dataservice.getCategories()
    .subscribe(result => {
      this.category = result;

      for (let i = 0; i < this.category.length; i++) {
        const element = this.category[i];
        if(element._id === this.data.category) {
          this.categorySelected = element.name;
        }
        if(this.data.category === ' ') {
          this.categorySelected = 'Sin asignar';
        }
      }

      console.log('category', this.categorySelected);
      this.loadingService.loadingDismiss();
    })
  }

  changeCategory(event) {
    if(event.target.value && event.target.value != '') {
      for (let i = 0; i < this.category.length; i++) {
        const element = this.category[i]._id;
        if(element === event.target.value) {
          this.categorySelected = this.category[i].name;
          console.log(event.target.value);
        }
      }      
    }
  }

  getListStateCity(user) {
    this.dataservice.getListCountry()
    .subscribe(result => {
      this.listStateCity = result;

      this.dataservice.buscarInfoCommercebyUser(user)
      .subscribe(res => {
        this.stateSelected = res['commerce'].state;
        this.citySelected = res['commerce'].city;
        console.log(this.commerce, this.stateSelected, this.citySelected);

        if(this.stateSelected === 'Desconocido' && this.citySelected === 'Desconocido') {
          this.cityArray = this.listStateCity[0].ciudades;
        }else {
          let c = this.listStateCity.filter(item => item.estado === this.stateSelected);
          this.cityArray = c[0].ciudades;
        }
      })
    });
  }

  changeState(event) {
    if(event.target.value && event.target.value != '') {
      this.stateSelected = event.target.value;
      if(this.stateSelected != 'Desconocido') {
        let c = this.listStateCity.filter(item => item.estado === this.stateSelected)
        this.cityArray = c[0].ciudades;
        this.citySelected = c[0].ciudades[0];
      }
    }
  }

  changeCity(event) {
    if(event.target.value && event.target.value != '') {
      this.citySelected = event.target.value;
    }
  }

  popPage() {
    this.navCtrl.pop();
  }

  cropImage(fileUrl) {
    this.crop.crop(fileUrl, { 
        quality: 100,
        targetWidth: 699,
        targetHeight: 700
    }).then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          console.log('Error cropping image' + error);
        }
      );
  }

  showCroppedImage(ImagePath) {
    this.isImage = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagePath = base64;
      this.picture = this.croppedImagePath;
      this.isImage = false;
    }, error => {
      console.log('Error in showing image' + error);
      this.isImage = false;
    });
  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 699,
      targetHeight: 700,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData)
    }, (err) => {
      console.error(err);
    });
  }

  //Actualizar datos del comercio
  updateInfoCommerce() {
    if(this.picture) {
      this.loadingService.loadingImage();
      this.dataservice.uploadImage(this.picture)
      .subscribe(imageData => {
        this.newPicture = imageData;
        console.log('filebase', this.newPicture);
        this.dataservice.updateCommerceData(this.commerce, this.dataC.name, this.dataC.description, this.dataC.rif, 
          this.data.category, this.dataC.address, this.stateSelected, this.citySelected, this.newPicture.url, this.dataC.phone)
        .subscribe(async result => {
          let res = result;
          this.loadingService.loadingDismiss();
          this.myToast = this.toastCtrl.create({
            message: "Su perfil ha sido actualizado con éxito",
            duration: 1000,
            color: "success",
            position: "top"
          }).then((toastData) => {
            console.log(toastData);
            toastData.present();
          });
  
          this.popPage();
          console.log('result update con imagen', res);
        });
      })  
    } else {
      this.loadingService.loadingPresent();
      this.dataservice.updateCommerceDataNoImage(this.commerce, this.dataC.name, this.dataC.description, this.dataC.rif, 
        this.data.category, this.dataC.address, this.stateSelected, this.citySelected, this.dataC.phone)
      .subscribe(async result => {
        let res = result;
        this.loadingService.loadingDismiss();
        this.myToast = this.toastCtrl.create({
          message: "Su perfil ha sido actualizado con éxito",
          duration: 1000,
          color: "success",
          position: "top"
        }).then((toastData) => {
          console.log(toastData);
          toastData.present();
        });

        this.popPage();
        console.log('result update sin imagen', res);
      });      
    }
  }

}
