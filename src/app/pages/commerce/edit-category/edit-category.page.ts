import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.page.html',
  styleUrls: ['./edit-category.page.scss'],
})
export class EditCategoryPage implements OnInit {

  menu: string;
  commerce: string;
  listMenuC: any = [];
  selectedMenu: any;
  menuID : any;
  myToast: any;

  constructor(private dataservice: DataService, private navCtrl: NavController, private toastCtrl: ToastController,
      private activatedRoute: ActivatedRoute, private loadingService: LoadingService, private auth: AuthenticationService, 
      public alertCtrl: AlertController) { }

  ngOnInit() {
    this.menu = this.activatedRoute.snapshot.queryParamMap.get('id');
    this.commerce = this.activatedRoute.snapshot.queryParamMap.get('commerce');
    this.showMenuCommerce();
  }

  popPage() {
    this.navCtrl.pop();
  }

  showMenuCommerce() {
    this.loadingService.loadingPresent();
    this.dataservice.listMenuCommerce(this.commerce)
    .subscribe(result => {
      this.listMenuC = result;
      this.loadingService.loadingDismiss();

      let arraymenu = this.listMenuC.filter(
        (a) => a._id === this.menu
      );
      this.selectedMenu = arraymenu[0].name;
      this.menuID = arraymenu[0].id;
      console.log(this.listMenuC, this.menuID);
    },
    async (error: HttpErrorResponse) => {
      //console.log(error)
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });
  }

  updateMenu() {
     this.dataservice.updateMenu(this.menu, this.selectedMenu, 0, this.menuID, this.commerce)
    .subscribe(result => {
      this.selectedMenu = result;
      this.popPage();
      this.myToast = this.toastCtrl.create({
        message: "El menú ha sido actualizado",
        duration: 1000,
        color: "success",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
      console.log('menu', this.selectedMenu);
    });
  }

}
