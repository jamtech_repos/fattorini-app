import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';
import * as numeral from 'numeral';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.page.html',
  styleUrls: ['./promotion.page.scss'],
})
export class PromotionPage implements OnInit {

  hour: string = '';
  promos : any = [];
  user: any = [];
  isSkeleton : boolean = false;
  textoBuscar = '';
  currency: string = '';

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, private http: HttpClient, private storage: Storage) { }

  ngOnInit() {
    // Obtiene por parámetro la hora actual para la validación de los horarios del comercio
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('ionViewWillEnter currency', this.currency);
      }
    } ));
    this.getUserLogged();
    console.log('ionViewWillEnter destacados');
  }

  listPromos(city) {
    this.dataservice.listPromo(city)
    .subscribe(res => {
      this.isSkeleton = true;
      this.promos = res;
      console.log(this.promos, 'destacados');
    })    
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  getCountPromos() {
    let count = 0;
    count = this.promos.length;
    //console.log('count', count)
    return count;
  }

  getOffer(price, percent) {
    let res = price * (percent / 100);
    let result = Number.parseFloat(res.toString()).toFixed(2);
    return price - parseFloat(result);
  }

  priceFormat(price) {
    return numeral(price).format('0,0.00');
  }

  priceFormatTasa(price) {
    return numeral(price).format('0,0').replace(/,/g,'.');
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    let result: string = await this.storage.get('token');
    const headers = new HttpHeaders().set('Authorization', result);
    this.http.get(SERVER_URL + 'user/loggedin', {headers})
    .subscribe(result => {
        this.user = result;
        this.listPromos(this.user.city);
        console.log('dataUser', this.user);
      },
      err => {
        console.log('ERROR!: ', err);
      }
    );
  } 

}
