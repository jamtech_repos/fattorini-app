import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.page.html',
  styleUrls: ['./favorite.page.scss'],
})
export class FavoritePage implements OnInit {

  fav: any[] = [];
  open: any[] = [];
  isSkeleton: boolean = false;
  refresh: any;

  constructor(private storage: Storage, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    setTimeout(() => {
    this.getFavorite();
    this.isSkeleton = true;
    }, 2000);
    console.log('entre');
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getFavorite();
      event.target.complete();
    }, 2000);
  }

  ionViewWillLeave() {
    clearInterval(this.refresh);
  }

  getFavorite() {
    this.storage.get('profile').then((val => {
      this.fav = val;
      console.log('data', this.fav);
    }))

    this.storage.get('open').then((val => {
      this.open = val;
      console.log('open', this.fav);
    }))
  }

}
