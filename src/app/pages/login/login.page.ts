import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { AlertController, MenuController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials = {
    email: '',
    password: ''
  };

  hour : string = '';
  showPassword = false;
  passwordToggleIcon = 'eye-outline';
 
  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private alertCtrl: AlertController,
    private storage: Storage,
    private loadingService: LoadingService,
    private menuCtrl: MenuController
  ) {}

  togglePassword():void {
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye-outline') {
      this.passwordToggleIcon = 'eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }
  }
 
  ngOnInit() {}

  ionViewDidEnter(){
    this.menuCtrl.enable(false);
  }

  ionViewWillLeave(){
    this.menuCtrl.enable(true);
  }
 
  login() {
    this.loadingService.loadingPresent();
    this.auth.login(this.credentials).subscribe(async res => {
      if (res) {
        let storageObs = this.storage.set('token', res.token);
        let storageRole = this.storage.set('role', res.role);
        let currencyStorage = this.storage.set('currency', 'USD');
        console.log('currencyStorage login init', currencyStorage);
        this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');
        this.loadingService.loadingDismiss();
        console.log(storageObs, storageRole, res, 'data');
        if(res.role === 'CA') {
          this.router.navigate(['/commerce'], { queryParams: { hour: this.hour, currency: 'USD' } });
        } else if(res.role === 'C') {
          this.router.navigate(['/users'], { queryParams: { hour: this.hour, currency: 'USD' } });
        } else if(res.role === 'A') {
          this.router.navigate(['/admin'], { queryParams: { hour: this.hour, currency: 'USD' } });
        }
      }
    }, async (error: Response) => {
      this.loadingService.loadingDismiss();
      if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: ['OK']
        });
        await alert.present();
      } else if(error.status === 400) {
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: 'Usuario o contraseña inválidos',
          buttons: ['OK']
        });
        await alert.present();
      }
     else if(error.status === 404) {
      const alert = await this.alertCtrl.create({
        header: 'Error',
        message: 'El usuario ingresado no se encuentra registrado o ha sido dado de baja',
        buttons: ['OK']
      });
      await alert.present();
    }
      else {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'No hemos podido conectarnos con el servidor, intente más tarde',
          buttons: ['OK']
        });
        await alert.present();   
        console.log(error);
      }
    });
  }
 
}

