import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { DataService } from 'src/app/services/data.service';
import { AlertController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  nationality: any[] = [
    {
      name: 'V',
      
    },
    {
      name: 'E',
    },
  ];

  data = {
    firstname : '',
    lastname : '',
    birthday : '1990-01-01T13:47:20.789-04:00',
    email : '',
    role : 'C',
    ci : '',
    status : true,
    phone : '',
    password : ''
  }

  selectedCI = '';
  selectedNationality = 'V';
  age18 : any;
  showPassword = false;
  passwordToggleIcon = 'eye-outline';
  isChecked : boolean = false;

  constructor(private dataservice: DataService, private alertCtrl: AlertController, private router: Router, 
              private loadingService: LoadingService, private menuCtrl: MenuController) { }

  ngOnInit() {
    this.age18 = moment(new Date()).subtract(18, 'years').format("YYYY-MM-DD");
    console.log(this.age18);
  }

  ionViewDidEnter(){
    this.menuCtrl.enable(false);
  }

  ionViewWillLeave(){
    this.menuCtrl.enable(true);
  }

  togglePassword():void {
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye-outline') {
      this.passwordToggleIcon = 'eye-off-outline';
    } else {
      this.passwordToggleIcon = 'eye-outline';
    }
  }

  changeNationality(event) {
    this.selectedNationality = event.detail.value;
    console.log(this.selectedNationality, 'nacionalidad');
  }

  cambioFecha(event) {
    const date = event.detail.value.split("T")[0];
    const date2 = date.replace(/\-/g, '');
    var years = moment().diff(date2, 'years', false);
    this.data.birthday = event.detail.value;
    console.log(this.data.birthday);
  }

  register() {
    this.loadingService.loadingPresent();
    this.data.ci = this.selectedNationality + '-' + this.selectedCI;
    console.log(this.data.ci);
    this.dataservice.registerData(this.data).subscribe( async res => {
      this.loadingService.loadingDismiss();
      const alert = await this.alertCtrl.create({
        header: 'Registro completado',
        message: 'Inicie sesión para acceder a su cuenta',
        buttons: ['OK']
      });
      await alert.present();
      this.router.navigateByUrl('/login');
    },
    async (error: Response) => {
      this.loadingService.loadingDismiss();
      if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: ['OK']
        });
        await alert.present();
      }
      else if(error.status === 403) {
        const alert = await this.alertCtrl.create({
          header: 'Registro fallido',
          message: 'Ya existe una cuenta con este correo electrónico',
          buttons: ['OK']
        });
        await alert.present();
      }
      else {
        const alert = await this.alertCtrl.create({
          header: 'Registro fallido',
          message: 'No hemos podido registrar su usuario, intente más tarde',
          buttons: ['OK']
        });
        await alert.present();   
      }
      console.log(error);
    })
  }

  changeTerms(event) {
    if (this.isChecked) {
      event.checked = true;
      console.log(this.isChecked);
    } else {
      event.checked = false;
      console.log(this.isChecked);
    }
  }

}