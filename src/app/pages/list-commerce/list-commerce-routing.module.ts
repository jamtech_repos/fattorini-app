import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListCommercePage } from './list-commerce.page';

const routes: Routes = [
  {
    path: '',
    component: ListCommercePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListCommercePageRoutingModule {}
