import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListCommercePageRoutingModule } from './list-commerce-routing.module';

import { ListCommercePage } from './list-commerce.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListCommercePageRoutingModule,
    PipesModule
  ],
  declarations: [ListCommercePage]
})
export class ListCommercePageModule {}
