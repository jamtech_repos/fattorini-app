import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-list-commerce',
  templateUrl: './list-commerce.page.html',
  styleUrls: ['./list-commerce.page.scss'],
})
export class ListCommercePage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;

  page = 0;
  idCategory : string = '';
  hour: string = '';
  user: any = [];
  currency: string = '';
  textoBuscar = '';
  dataP : any[] = [];
  isSkeleton : boolean = false;
  commerce : any[] = [];

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute,
              public storage: Storage, private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.idCategory = this.activatedRoute.snapshot.queryParamMap.get('category');
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
    this.user = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.listCommercebyCat(false, "")
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency dashboard', this.currency);
      }
    } ));
  }

  getCountCommerce() {
    let count = 0;
    count = this.commerce.length;
    //console.log('count', count)
    return count;
  }

  listCommercebyCat(isFirstLoad, event) {
    this.dataservice.getListCommercebyCategory(this.idCategory, this.page, this.hour)
    .subscribe(result => {
      this.isSkeleton = true;
      this.dataP = result;

      let maximumPage = this.dataP['pages'];

      for (const commerce of result['commerces']) {
        this.commerce.push(commerce.commerce);
      }

      if(this.page < maximumPage || this.page === 0){
        if(isFirstLoad)
          event.target.complete();
          this.page++;
      } else
          this.infiniteScroll.disabled = true;

        console.log(this.commerce, 'commerce');
        console.log('page', this.page);
        console.log('max page', this.dataP);
        console.log(`page=${this.page}, max=${maximumPage}`);
    },
    async (error: HttpErrorResponse) => {
       if(error.status === 404) {
        const alert = await this.alertCtrl.create({
          header: 'Su sesión expiró',
          message: 'Inicie sesión nuevamente',
          buttons: [{
            text: 'OK',
            handler: data => {
              this.auth.logout();
            }
          }]
        });
        await alert.present();
      }
      else if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: [{
            text: 'SALIR',
            handler: data => {
              navigator['app'].exitApp();
            }
          }]
        });
        await alert.present();
      }
      console.log(error);
    });     
  }

  loadMoreCommerce(event) {
    this.listCommercebyCat(true, event);
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

}
