import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TopratedPageRoutingModule } from './toprated-routing.module';

import { TopratedPage } from './toprated.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TopratedPageRoutingModule,
    PipesModule,
  ],
  declarations: [TopratedPage]
})
export class TopratedPageModule {}
