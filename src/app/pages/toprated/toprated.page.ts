import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DataService } from 'src/app/services/data.service';
import { SERVER_URL } from 'src/environments/environment';

@Component({
  selector: 'app-toprated',
  templateUrl: './toprated.page.html',
  styleUrls: ['./toprated.page.scss'],
})
export class TopratedPage implements OnInit {

  hour: string = '';
  popular : any = [];
  user: any = [];
  isSkeleton : boolean = false;
  textoBuscar = '';
  currency: string = '';

  constructor(private dataservice: DataService, private activatedRoute: ActivatedRoute, private http: HttpClient, private storage: Storage) { }

  ngOnInit() {
    // Obtiene por parámetro la hora actual para la validación de los horarios del comercio
    this.hour = this.activatedRoute.snapshot.queryParamMap.get('hour');
  }

  ionViewWillEnter() {
    // Obtener el valor del cambio de moneda del selector
    this.storage.get('currency').then((val => { 
      if(val) {
        this.currency = val;
        console.log('currency dashboard', this.currency);
      }
    } ));
    this.getUserLogged();
    console.log('ionViewWillEnter toprated');
  }

  // Mostrar los comercios mejor calificados
  showBestQualified(city)
  {
    this.dataservice.getPopularCommerce(city, this.hour)
    .subscribe(result => {
      this.isSkeleton = true;
      this.popular = result;
      console.log(this.popular, 'popular');
    })
  }

  buscar(event) {
    this.textoBuscar = event.target.value;
  }

  getCountCommerce() {
    let count = 0;
    count = this.popular.length;
    //console.log('count', count)
    return count;
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    let result: string = await this.storage.get('token');
    const headers = new HttpHeaders().set('Authorization', result);
    this.http.get(SERVER_URL + 'user/loggedin', {headers})
    .subscribe(result => {
        this.user = result;
        this.showBestQualified(this.user.city);
        console.log('dataUser', this.user);
      },
      err => {
        console.log('ERROR!: ', err);
      }
    );
  } 

}
