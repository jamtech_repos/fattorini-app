import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController } from '@ionic/angular';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SERVER_URL } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  currency: string = '';
  user: any = [];

  dashboardOptions: any[] = [
    {
      id: 1,
      name: 'Categorías',
    },
/*     {
      id: 2,
      name: 'Productos',
    },
    {
      id: 3,
      name: 'Pedidos',
    }, */
    {
      id: 4,
      name: 'Editar perfil',
    }
  ];

  constructor(public popoverCtrl: PopoverController, private http: HttpClient, private storage: Storage,
              private router: Router, private alertCtrl: AlertController, private auth: AuthenticationService) { }

  ngOnInit() {
    this.getUserLogged();
  }

  onClick(value) {
    console.log('item', value);

    this.popoverCtrl.dismiss( {
      item : value
    });

    if(value === 'Categorías'){
      this.router.navigateByUrl('/category-dashboard');
/*     }else if(value === 'Productos') {
      this.router.navigateByUrl('/products-dashboard'); */
/*     }else if(value === 'Pedidos') {
      this.router.navigateByUrl('/orders-dashboard'); */
    }else if(value === 'Editar perfil') {
      this.router.navigateByUrl('/profile-dashboard');
      //this.router.navigate(['/profile-dashboard'], { queryParams: { id: this.user._id } });
    }
  }

  // Obtener datos de usuario logueado
  async getUserLogged() {
    if(this.storage.get('token')) {
      let result: string = await this.storage.get('token');
      const headers = new HttpHeaders().set('Authorization', result);
      this.http.get(SERVER_URL + 'user/loggedin', {headers})
      .subscribe(result => {
          this.user = result;
        },
        async (error: HttpErrorResponse) => {
          //console.log(error)
           if(error.status === 404) {
            const alert = await this.alertCtrl.create({
              header: 'Su sesión expiró',
              message: 'Inicie sesión nuevamente',
              buttons: [{
                text: 'OK',
                handler: data => {
                  this.auth.logout();
                }
              }]
            });
            await alert.present();
          }
          else if(error.status === 0) {
            const alert = await this.alertCtrl.create({
              header: 'Fallo de conexión',
              message: 'Verifique su conexión a Internet e intente de nuevo',
              buttons: [{
                text: 'SALIR',
                handler: data => {
                  navigator['app'].exitApp();
                }
              }]
            });
            await alert.present();
          }
          console.log(error);
        });  
    }
  } 

}
