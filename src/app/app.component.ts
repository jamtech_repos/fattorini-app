import { Component, ViewChild } from '@angular/core';

import { Platform, IonRouterOutlet, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { Network } from '@ionic-native/network/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  users : string = '/users';
  commerce : string = '/commerce';
  showSplash = true;

  @ViewChild('myRouterOutlet', { static: true })routerOutlet: IonRouterOutlet;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private auth: AuthenticationService,
    public network: Network,
    public alertCtrl: AlertController,
    private oneSignal: OneSignal
  ) {
    this.initializeApp();
    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url.indexOf(this.users) == 0 || this.router.url.indexOf(this.commerce) == 0 || this.router.url === '/login') {
        navigator['app'].exitApp();
      } else {
        //this.presentAlertConfirm()
      }
    });
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Falla de conexión',
      message: 'No hay conexión a Internet, por favor intente más tarde',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            console.log('Yes clicked');
            navigator['app'].exitApp();
          }
        }
      ],
      backdropDismiss: false,
    });
      await alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      if (this.platform.is('android')) {
        this.statusBar.backgroundColorByName('white');
        this.statusBar.styleDefault();
      }

    // watch network for a disconnection
    this.network.onDisconnect().subscribe(() => {
      this.presentAlert();
      console.log('no network');
    });

    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });

      this.splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false)

      if (this.platform.is('cordova')) {
        this.setupPush();
      }

      this.auth.user.subscribe(state => {
        console.log('Auth changed: ', state);
      });

    });
  }

  setupPush() {
    this.oneSignal.startInit('d6702725-e3d6-49f6-987b-8bccb38234ea', '966431888120');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
    this.oneSignal.endInit();
  }

}
