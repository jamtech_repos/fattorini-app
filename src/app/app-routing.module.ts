import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './services/login.guard';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    canActivate: [LoginGuard],
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    canActivate: [LoginGuard],
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'users',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/users/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'commerce',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'popover',
    loadChildren: () => import('./pages/popover/popover.module').then( m => m.PopoverPageModule)
  },
  {
    path: 'detail-commerce',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/detail-commerce/detail-commerce.module').then( m => m.DetailCommercePageModule)
  },
  {
    path: 'detail-product/:id/:commerce/:menu',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/detail-product/detail-product.module').then( m => m.DetailProductPageModule)
  },
  {
    path: 'cart',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'favorite',
    loadChildren: () => import('./pages/favorite/favorite.module').then( m => m.FavoritePageModule)
  },
  {
    path: 'category',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/category/category.module').then( m => m.CategoryPageModule)
  },
  {
    path: 'currencyselector',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/currencyselector/currencyselector.module').then( m => m.CurrencyselectorPageModule)
  },
  {
    path: 'list-commerce',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/list-commerce/list-commerce.module').then( m => m.ListCommercePageModule)
  },
  {
    path: 'profile',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'seller',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/seller/seller.module').then( m => m.SellerPageModule)
  },
  {
    path: 'category-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/category-dashboard/category-dashboard.module').then( m => m.CategoryDashboardPageModule)
  },
  {
    path: 'products-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/products-dashboard/products-dashboard.module').then( m => m.ProductsDashboardPageModule)
  },
  {
    path: 'orders-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/orders-dashboard/orders-dashboard.module').then( m => m.OrdersDashboardPageModule)
  },
  {
    path: 'profile-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/profile-dashboard/profile-dashboard.module').then( m => m.ProfileDashboardPageModule)
  },
  {
    path: 'filtermenu',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/filtermenu/filtermenu.module').then( m => m.FiltermenuPageModule)
  },
  {
    path: 'productsmenu',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/productsmenu/productsmenu.module').then( m => m.ProductsmenuPageModule)
  },
  {
    path: 'exchangerate',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/exchangerate/exchangerate.module').then( m => m.ExchangeratePageModule)
  },
  {
    path: 'addcategory',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/addcategory/addcategory.module').then( m => m.AddcategoryPageModule)
  },
  {
    path: 'edit-category',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/edit-category/edit-category.module').then( m => m.EditCategoryPageModule)
  },
  {
    path: 'address',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/address/address.module').then( m => m.AddressPageModule)
  },
  {
    path: 'addaddress',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/addaddress/addaddress.module').then( m => m.AddaddressPageModule)
  },
  {
    path: 'edit-address',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/edit-address/edit-address.module').then( m => m.EditAddressPageModule)
  },
  {
    path: 'orderspanel',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/orders/orderspanel/orderspanel.module').then( m => m.OrderspanelPageModule)
  },
  {
    path: 'completeorder',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/orders/completeorder/completeorder.module').then( m => m.CompleteorderPageModule)
  },
  {
    path: 'orderpayment',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/orders/orderpayment/orderpayment.module').then( m => m.OrderpaymentPageModule)
  },
  {
    path: 'ordershipping',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/orders/ordershipping/ordershipping.module').then( m => m.OrdershippingPageModule)
  },
  {
    path: 'orderdetails',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/orders/orderdetails/orderdetails.module').then( m => m.OrderdetailsPageModule)
  },
  {
    path: 'orders-detail-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/orders-detail-dashboard/orders-detail-dashboard.module').then( m => m.OrdersDetailDashboardPageModule)
  },
  {
    path: 'orders-status-dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/commerce/orders-status-dashboard/orders-status-dashboard.module').then( m => m.OrdersStatusDashboardPageModule)
  },
  {
    path: 'user-comments',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/comments/user-comments/user-comments.module').then( m => m.UserCommentsPageModule)
  },
  {
    path: 'addcomment',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/comments/addcomment/addcomment.module').then( m => m.AddcommentPageModule)
  },
  {
    path: 'tarifas',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/tarifas/tarifas.module').then( m => m.TarifasPageModule)
  },
  {
    path: 'admin',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminPageModule)
  },
  {
    path: 'reviews',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/reviews/reviews.module').then( m => m.ReviewsPageModule)
  },
  {
    path: 'promotion',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/promotion/promotion.module').then( m => m.PromotionPageModule)
  },
  {
    path: 'toprated',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/toprated/toprated.module').then( m => m.TopratedPageModule)
  },
  {
    path: 'vendorcity',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./pages/vendorcity/vendorcity.module').then( m => m.VendorcityPageModule)
  },  {
    path: 'search',
    loadChildren: () => import('./pages/search/search.module').then( m => m.SearchPageModule)
  }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
