import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate{

  token : any;
  role: any;
  currency: any;
  hour: string;
  
  constructor(private router: Router, private alertCtrl: AlertController, private storage: Storage) { }
 
  async canActivate(): Promise<boolean> {
    this.token = await this.storage.get('token').then((val => {
      return val;
    }));

    this.role = await this.storage.get('role').then((res => {
      return res;
    }));

    this.currency = await this.storage.get('currency').then((res => {
      return res;
    }));

    this.hour = (moment().format('h:mm a')).toString().replace(/ /g, '+');

    if (this.token) {
/*        this.alertCtrl.create({
        header: 'Unauthorized',
        message: 'You are not allowed to access that page.',
        buttons: ['OK']
      }).then(alert => alert.present()); */

      if(this.role === 'CA') {
        this.router.navigate(['/commerce'], { queryParams: { hour: this.hour, currency: this.currency } });
      } else if(this.role === 'C') {
        this.router.navigate(['/users'], { queryParams: { hour: this.hour, currency: this.currency } });
      } else if(this.role === 'A') {
        this.router.navigate(['/admin'], { queryParams: { hour: this.hour, currency: this.currency } });
      }
    
      return false;
    } else {
      return true;
    }
  }
}
