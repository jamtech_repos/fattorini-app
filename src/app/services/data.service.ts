import { Injectable } from '@angular/core';
import { SERVER_URL } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  //Registro
  registerData(data) {
    return this.http.post<any[]>(SERVER_URL + 'user/register', data);
  }

  //Listar comercios por la ciudad del usuario
  getCommercebyCity(city, hour) {
    return this.http.get<any[]>(SERVER_URL + 'commerce/list/city?city=' + city + '&hour=' + hour)
    .pipe(
      delay(1000)
    );
  }

  //Configuración del comercio
  getConfigCommerce(id) {
    return this.http.get<any[]>(SERVER_URL + 'commerce_config?commerce=' + id);
  }

  //Cambio de la tasa del dolar
  updateTasaDolar(commerce, exchange_rate) {
    var data = {
      body: 
      { exchange_rate: exchange_rate },
      params: 
      { commerceId: commerce }
    }
    return this.http.put<any[]>(SERVER_URL + 'commerce_config', data, { observe : "response" });
  }  

  //Crear categoría menú
  createMenu(name, depth, id, commerce) {
    var data = {
      name: name,
      depth: depth,
      id: id,
      commerce: commerce
    }
    return this.http.post<any[]>(SERVER_URL + 'menu', data, { observe : "response" });
  }
  
  //Actualizar categoría menú
  updateMenu(menu, name, depth, id, commerce) {
    var data = {
      name: name,
      depth: depth,
      id: id,
      commerce: commerce
    }
    return this.http.put<any[]>(SERVER_URL + 'menu?id=' + menu, data, { observe : "response" });
  } 

  //Actualizar usuario
   updateUserData(id, firstname, lastname, address, password, state, city, phone, birthday, ci) {
    var data = {
      firstname : firstname,
      lastname : lastname,
      address : address,
      password : password,
      state : state,
      city : city,
      ci : ci,
      phone : phone,
      birthday : birthday
    }
    return this.http.put<any[]>(SERVER_URL + 'user/update?id=' + id, data);
  }

  //Actualizar información del comercio con imagen
  updateCommerceData(id, name, description, rif, category, address, state, city, picture, phone) {
    var data = {
      name : name,
      description : description,
      rif : rif,
      category : category,
      address : address,
      phone: phone,
      state : state,
      city : city,
      picture : picture
    }
    return this.http.put<any[]>(SERVER_URL + 'commerce?id=' + id, data);
  }

  //Actualizar información del comercio sin imagen
  updateCommerceDataNoImage(id, name, description, rif, category, address, state, city, phone) {
    var data = {
      name : name,
      description : description,
      rif : rif,
      category : category,
      address : address,
      state : state,
      city : city,
      phone : phone
    }
    return this.http.put<any[]>(SERVER_URL + 'commerce?id=' + id, data);
  }

  buscarInfoCommercebyUser(id) {
    return this.http.get<any[]>(SERVER_URL + 'admins/commerce?user=' + id);
  }

  //Listado de estado/ciudad
  getListCountry() {
    return this.http.get<any[]>(SERVER_URL + 'country/venezuela');
  }

  //Subida de imágenes
  uploadImage(base) {
    var data = {
      base : base
    }
    return this.http.post<any[]>('https://fileapi.appfattorini.com/file/upload/base64', data);
  }

  //Registrar usuario como comercio
  updateRegister(id, name, description, rif, category, address, state, city, picture, phone) {
    var data = {
      name : name,
      description : description,
      rif : rif,
      category : category,
      address : address,
      state : state,
      city : city,
      picture : picture,
      phone : phone
    }
    return this.http.put<any[]>(SERVER_URL + 'user/updateregister/CA/?userid=' + id, data);
  }

  //Listar iconos de categorías en el home
  listCategoryHome(page) {
    return this.http.get<any[]>(SERVER_URL + 'category/list/actives?page='+ page + '&perPage=15')
     .pipe(
      delay(1000)
    );
  }

  //Listar categorías en el Ver más del home
  getListCategories(page) {
    return this.http.get<any[]>(SERVER_URL + 'category/list/actives?page='+ page + '&perPage=10')
     .pipe(
      delay(1000)
    );
  }

  //Listar categorías sin paginación
  getCategories() {
    return this.http.get<any[]>(SERVER_URL + 'category/list')
     .pipe(
      delay(1000)
    );
  }

  //Listar comercios según su categoría
  getListCommercebyCategory(category, page ,hour) {
    return this.http.get<any[]>(SERVER_URL + 'commerce/list/category?category='+ category + '&page=' + page + '&perPage=15' + '&hour' + hour)
     .pipe(
      delay(1000)
    );    
  }

  //Listar comercios en el home si estan activos
  listCommerce(hour) {
    return this.http.get<any[]>(SERVER_URL + 'commerce/list/status?status=2' + '&hour=' + hour)
     .pipe(
      delay(1000)
    );
  }

  //Información del comercio para el detail-commerce
  listInfoCommerceComplete(id, hour, currency)  {
    return this.http.get<any[]>(SERVER_URL + 'commerce/complete?id=' + id + '&hour=' + hour + '&currency=' + currency)
      .pipe(
      delay(1000)
    ); 
  }

  //Listar productos del comercio
  listProductCommerce(id, page, hour, currency) {
    return this.http.get<any[]>(SERVER_URL + 'product/commerce?_id=' + id + '&page=' + page + '&perPage=15' + '&hour=' + hour + '&currency=' + currency)
      .pipe(
      delay(1000)
    );  
  }
 
  //Listar el menu de los productos del comercio
  listProductMenuCommerce(id)  {
    return this.http.get<any[]>(SERVER_URL + 'product/commerce?commerce=' + id)
      .pipe(
      delay(1000)
    );
  }

  //Listar productos por el menú
  getProductMenu(id, page, currency)  {
    return this.http.get<any[]>(SERVER_URL + 'product/menu?menu=' + id + '&page=' + page + '&perPage=15' + '&currency=' + currency)
      .pipe(
      delay(1000)
    );
  }  

  //Listar el menu del comercio
  listMenuCommerce(id)  {
    return this.http.get<any[]>(SERVER_URL + 'menu/list?commerce=' + id)
      .pipe(
      delay(1000)
    );
  }

  //Información del producto para el detail-product
  getProductInfo(id, currency)  {
    return this.http.get<any[]>(SERVER_URL + 'product?id=' + id + '&currency=' + currency)
      .pipe(
      delay(1000)
    );
  }

  //Actulizar el producto si esta disponible o no
  updateStockStatus(id, stock_status)  {
    var data = {
      stock_status: stock_status
    }
    return this.http.put<any[]>(SERVER_URL + 'product?id=' + id, data);
  }

  //Listar productos relacionados según el menú
  listProductMenuRelated(id, currency)  {
    return this.http.get<any[]>(SERVER_URL + 'product/menu?menu=' + id + '&currency=' + currency + '&page=0&perPage=4')
      .pipe(
      delay(1000)
    ); 
  }
  
  //Verificar si el producto ya esta en el carrito o no
  getProductCart(user, currency) {
    return this.http.get<any[]>(SERVER_URL + 'cart?user=' + user + '&currency=' + currency)
  }

  //Actualizar cantidad del producto en el carrito
  updateProductCart(id, quantity) {
    var data = {
      id : id,
      quantity : quantity
    }
    return this.http.put<any[]>(SERVER_URL + 'cart?id=' + id + '&quantity=' + quantity, data, { responseType: 'text' as 'json', observe : "response" })
  }

  //Añadir al carrito mediante el body de la petición
  addToCart(user, product, options, grams) {
    var data = {
      user : user,
      product : product,
      options : options,
      grams : grams
    }
    return this.http.post<any[]>(SERVER_URL + 'cart', data, { observe : "response" })
  }

  //Total de productos en el carrito
  getTotalCart(user) {
    return this.http.get<any[]>(SERVER_URL + 'cart/total?user=' + user)
  }

  //Eliminar producto del carrito
  removeProductCart(id) {
    return this.http.delete<any[]>(SERVER_URL + 'cart?id=' + id)
  }

  //Verificar si el producto ya esta en el carrito o no (RELATED)
  getProductInCart(user, product) {
    return this.http.get<any[]>(SERVER_URL + 'cart/product?user=' + user + '&product=' + product)
  }

  //Cambiar status del menú
  changeMenu(id, status) {
    var data = {
      status : status
    }
    return this.http.put<any[]>(SERVER_URL + 'menu?id=' + id, data)
  }
  
  //Listar configuracion general
  getAppConfig() {
    return this.http.get<any[]>(SERVER_URL + 'appconfig')
  } 

  ///////////////// Direcciones guardadas /////////////////

  //Listar direcciones
  getListUserAddress(user) {
    return this.http.get<any[]>(SERVER_URL + 'useraddress/user?user=' + user)
  }

  //Guardar direcciones
  addAddressUserProfile(user, state, city, sector, address) {
    var data = {
      user : user,
      state : state,
      city : city,
      sector : sector,
      address : address
    }
    return this.http.post<any[]>(SERVER_URL + 'useraddress', data, { observe : "response" })
  }

  //Eliminar dirección
  removeAddress(id) {
    return this.http.delete<any[]>(SERVER_URL + 'useraddress?id=' + id)
  }

  //Actualizar cantidad del producto en el carrito
  updateAddressUser(id, state, city, sector, address) {
    var data = {
      state : state,
      city : city,
      sector : sector,
      address : address
    }
    return this.http.put<any[]>(SERVER_URL + 'useraddress?id=' + id, data, { responseType: 'text' as 'json', observe : "response" })
  }

  ///////////////// Pedidos usuario /////////////////

   //Listar pedidos
   getListUserOrders(user, currency) {
    return this.http.get<any[]>(SERVER_URL + 'order/list/user?user=' + user + '&currency=' + currency)
  } 

  //Cambiar status de la orden
  changeStatusOrder(id, status) {
    return this.http.put<any[]>(SERVER_URL + 'order/status?status=' + status + '&id=' + id, { observe : "response" })
  }  

  //Crear pedido en el carrito
  createOrderUser(user, hour) {
    var data = {
      user : user,
      hour : hour
    }
    return this.http.post<any[]>(SERVER_URL + 'order', data, { observe : "response" })
  }

  //Detalle del pago
  getPaymentDetail(order, commerce, user, delivery_amount) {
    var data = {
      order : order,
      commerce : commerce,
      user : user,
      delivery_amount : delivery_amount
    }
    return this.http.post<any[]>(SERVER_URL + 'paymentdetail', data, { observe : "response" })
  }
  
  //Detalle del pedido
  getOrderDetails(order, currency) {
    return this.http.get<any[]>(SERVER_URL + 'order?id=' + order + '&currency=' + currency)
  } 

  //Detalle del pedido
  getOrderPaymentDetail(order, currency) {
    return this.http.get<any[]>(SERVER_URL + 'paymentdetail/order?order=' + order + '&currency=' + currency)
  } 

  //Actualizar la orden al pasar al detalle del pago
  updateOrderShipping(order, delivery, status, state, city, sector, address, phone, note, name, rif) {
    var data = {
      delivery : delivery,
      status : status,
      state : state,
      city : city,
      sector : sector,
      address : address,
      phone : phone,
      note : note,
      name : name,
      rif : rif,
    }
    return this.http.put<any[]>(SERVER_URL + 'order?id=' + order, data, { responseType: 'text' as 'json', observe : "response" })
  }

  //Listar metodos de pago del comercio
  getBankData(commerce) {
    return this.http.get<any[]>(SERVER_URL + 'bankdata?commerce=' + commerce)
  }

  //Finalizar compra (status = 1)
  orderReady(id, bankdata, proof_of_payment, reference, status, name, email, description) {
    var data = {
      bankdata : bankdata,
      proof_of_payment : proof_of_payment,
      reference : reference,
      status : status,
      name : name,
      email : email,
      description : description
    }
    return this.http.put<any[]>(SERVER_URL + 'paymentdetail?id=' + id, data, { responseType: 'text' as 'json', observe : "response" })
  }

  orderReadyPM(id, bankdata, proof_of_payment, status) {
    var data = {
      bankdata : bankdata,
      proof_of_payment : proof_of_payment,
      status : status
    }
    return this.http.put<any[]>(SERVER_URL + 'paymentdetail?id=' + id, data, { responseType: 'text' as 'json', observe : "response" })
  }

  //Actualizar status = 2 en la orden
  updateOrderStatus(order, status) {
    var data = {
      status : status
    }
    return this.http.put<any[]>(SERVER_URL + 'order?id=' + order, data, { responseType: 'text' as 'json', observe : "response" })
  }

  //Pedidos del comercio
  getOrderCommerce(commerce, currency) {
    return this.http.get<any[]>(SERVER_URL + 'order/list/commerce?commerce=' + commerce + '&currency=' + currency)
  }

  //Mostrar banners del slide
  getBanners() {
    return this.http.get<any[]>(SERVER_URL + 'banners/all')
  }

  ///////////////// RESEÑAS /////////////////
  getCommentsUser(id) {
    return this.http.get<any[]>(SERVER_URL + 'comment/list/user?user=' + id)
  }

  getCommentsCommerce(id) {
    return this.http.get<any[]>(SERVER_URL + 'comment/list/commerce?commerce=' + id)
  }

  //Agregar comentario
  addCommentUser(id, score, text, status) {
    var data = {
      score : score,
      text : text,
      status : status
    }
    return this.http.put<any[]>(SERVER_URL + 'comment?id=' + id, data, { responseType: 'text' as 'json', observe : "response" })
  }

  //Home mejor calificados
  getPopularCommerce(city, hour) {
    return this.http.get<any[]>(SERVER_URL + 'commerce/popular?city=' + city + '&hour=' + hour)
  }

  //Mostrar comercios (estado) por la IP del usuario
  getCommerceByIp(state, hour) {
    return this.http.get<any[]>(SERVER_URL + 'commerce/list/state?state=' + state + '&hour=' + hour)
  }

  getInfoIp() {
    return this.http.get<any[]>('https://ipapi.co/json/')
  }

  //Listar productos destacados o promociones
  listPromo(city) {
    return this.http.get<any[]>(SERVER_URL + 'product/popular?city='+ city)
  }

  //Consultar repartidores
   getDeliveryman() {
    return this.http.get<any[]>(SERVER_URL + 'deliveryman')
  }

  //Búsqueda por producto, comercio, categoría
  search(keyword) {
    return this.http.get<any[]>(SERVER_URL + 'search?search=' + keyword)
  } 

  //Saber si esta de baja un usuario
  getUserActive(id) {
    return this.http.get<any[]>(SERVER_URL + 'user/active/' + id)
  }  

}
