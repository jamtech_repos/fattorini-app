import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  token : any;
  myToast : any;

  constructor(private router: Router, private auth: AuthenticationService, private alertCtrl: AlertController, private storage: Storage,
              private toastCtrl: ToastController) { }
 
  async canActivate(): Promise<boolean> {
    this.token = await this.storage.get('token').then((val => {
      return val;
    }));
    if (!this.token) {
      this.myToast = this.toastCtrl.create({
        message: "Se ha perdido el acceso a su cuenta, inicie sesión nuevamente",
        duration: 2000,
        color: "dark",
        position: "bottom"
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
          
      this.router.navigateByUrl('/login');
      return false;
    } else {
      return true;
    }
  }
}
