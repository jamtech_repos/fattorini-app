import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  isLoading = false;
  constructor(
    public loadingController: LoadingController
  ) 
  { } 

  async loadingPresent() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Por favor espere...',
      spinner: 'crescent'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort loading'));
        }
      });
    });
  }

  async loadingImage() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Subiendo nueva imagen...',
      spinner: 'crescent'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort loading'));
        }
      });
    });
  }

  async loadingImageLogo() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Subiendo logo...',
      spinner: 'crescent'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort loading'));
        }
      });
    });
  }

  async loadingComprobante() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Estamos procesando tu pedido...',
      spinner: 'crescent'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort loading'));
        }
      });
    });
  }

  async loadingOrder() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando su pedido...',
      spinner: 'crescent'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort loading'));
        }
      });
    });
  }

  async loadingDismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('loading dismissed'));
  }

  async loadingDismissOrder() {
    this.isLoading = false;
    return await this.loadingController.getTop().then(a => {
      if (a) {
        this.loadingController.dismiss().then(() => console.log('loading dismissed'));
      }
    });
  }
}