import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtromenu'
})
export class FiltromenuPipe implements PipeTransform {

  removeAccents(value) {
      return value
          .replace(/á/g, 'a')            
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u');
  }

  transform(menu: any[], texto = ''): any[] {
    
    if(texto.length === 0) {
      console.log(menu);
      return menu;
    }

    texto = this.removeAccents(texto.toString().toLocaleLowerCase());

    return menu.filter( item => {
      return this.removeAccents(item.name).toString().toLocaleLowerCase().includes(texto);
    })
  }
}
