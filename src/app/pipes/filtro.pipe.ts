import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  removeAccents(value) {
      return value
          .replace(/á/g, 'a')            
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u');
  }

  transform(category: any[], texto = ''): any[] {
    
    if(texto.length === 0) {
      console.log(category);
      return category;
    }

    texto = this.removeAccents(texto.toString().toLocaleLowerCase());

    return category.filter( item => {
      return this.removeAccents(item.category.name).toString().toLocaleLowerCase().includes(texto);
    })
  }
}
