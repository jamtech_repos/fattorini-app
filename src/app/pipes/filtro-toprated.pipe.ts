import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroToprated'
})
export class FiltroTopratedPipe implements PipeTransform {

  removeAccents(value) {
    return value
        .replace(/á/g, 'a')            
        .replace(/é/g, 'e')
        .replace(/í/g, 'i')
        .replace(/ó/g, 'o')
        .replace(/ú/g, 'u');
}

transform(commerce: any[], texto = ''): any[] {
  
  if(texto.length === 0) {
    console.log(commerce);
    return commerce;
  }

  texto = this.removeAccents(texto.toString().toLocaleLowerCase());

  return commerce.filter( item => {
    return this.removeAccents(item.commerce.name).toString().toLocaleLowerCase().includes(texto)
  })
}

}
