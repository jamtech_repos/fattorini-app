import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroOrders'
})
export class FiltroOrdersPipe implements PipeTransform {

transform(orders: any[], texto = ''): any[] {
  
  if(texto.length === 0) {
    //console.log(orders);
    return orders;
  }

  texto = texto.toString().toLocaleLowerCase();

  return orders.filter( item => {
    return item.orderid.toString().toLocaleLowerCase().includes(texto);
  })
}

}
