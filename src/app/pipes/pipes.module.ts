import { NgModule } from '@angular/core';
import { FiltroPipe } from './filtro.pipe';
import { FiltroCommercePipe } from './filtro-commerce.pipe';
import { FiltromenuPipe } from './filtromenu.pipe';
import { FiltroproductsPipe } from './filtroproducts.pipe';
import { FiltroOrdersPipe } from './filtro-orders.pipe';
import { FiltroTopratedPipe } from './filtro-toprated.pipe';
import { FiltroPromosPipe } from './filtro-promos.pipe';



@NgModule({
  declarations: [FiltroPipe, FiltroCommercePipe, FiltromenuPipe, FiltroproductsPipe, FiltroOrdersPipe, FiltroTopratedPipe, FiltroPromosPipe],
  exports: [FiltroPipe, FiltroCommercePipe, FiltromenuPipe, FiltroproductsPipe, FiltroOrdersPipe, FiltroTopratedPipe, FiltroPromosPipe]
})
export class PipesModule { }
